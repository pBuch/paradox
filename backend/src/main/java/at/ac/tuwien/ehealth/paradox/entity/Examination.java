package at.ac.tuwien.ehealth.paradox.entity;

import javax.persistence.*;

@Entity
public class Examination {

    @Id
    @GeneratedValue
    private Long id;

    @Embedded
    private PatientPosition patientPosition;

    @Embedded
    private RespiratoryExamination respiratoryExamination;

    @Embedded
    private CirculatoryExamination circulatoryExamination;

    @Column
    private Boolean patientCritical;

    @Embedded
    private NeurologicalExamination neurologicalExamination;

    @Embedded
    private FurtherExamination furtherExamination;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PatientPosition getPatientPosition() {
        return patientPosition;
    }

    public void setPatientPosition(PatientPosition patientPosition) {
        this.patientPosition = patientPosition;
    }

    public RespiratoryExamination getRespiratoryExamination() {
        return respiratoryExamination;
    }

    public void setRespiratoryExamination(RespiratoryExamination respiratoryExamination) {
        this.respiratoryExamination = respiratoryExamination;
    }

    public CirculatoryExamination getCirculatoryExamination() {
        return circulatoryExamination;
    }

    public void setCirculatoryExamination(CirculatoryExamination circulatoryExamination) {
        this.circulatoryExamination = circulatoryExamination;
    }

    public Boolean getPatientCritical() {
        return patientCritical;
    }

    public void setPatientCritical(Boolean patientCritical) {
        this.patientCritical = patientCritical;
    }

    public NeurologicalExamination getNeurologicalExamination() {
        return neurologicalExamination;
    }

    public void setNeurologicalExamination(NeurologicalExamination neurologicalExamination) {
        this.neurologicalExamination = neurologicalExamination;
    }

    public FurtherExamination getFurtherExamination() {
        return furtherExamination;
    }

    public void setFurtherExamination(FurtherExamination furtherExamination) {
        this.furtherExamination = furtherExamination;
    }
}
