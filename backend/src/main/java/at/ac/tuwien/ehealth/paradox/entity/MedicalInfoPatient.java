package at.ac.tuwien.ehealth.paradox.entity;

import javax.persistence.*;

@Entity
public class MedicalInfoPatient {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String doctor;

    @Column
    private String specialInsuranceName;

    @Column
    private String specialInsurancePolicy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public String getSpecialInsuranceName() {
        return specialInsuranceName;
    }

    public void setSpecialInsuranceName(String specialInsuranceName) {
        this.specialInsuranceName = specialInsuranceName;
    }

    public String getSpecialInsurancePolicy() {
        return specialInsurancePolicy;
    }

    public void setSpecialInsurancePolicy(String specialInsurancePolicy) {
        this.specialInsurancePolicy = specialInsurancePolicy;
    }
}
