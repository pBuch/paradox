package at.ac.tuwien.ehealth.paradox.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CirculatoryExamination {

    @Column
    private Boolean strongBleeding;

    @Column
    private Boolean arrhythmia;

    @Column
    private Boolean shock;

    @Column
    private Boolean cardiacArrest;

    @Column
    private Boolean burning;

    @Column
    private Integer burningFirstDegree;

    @Column
    private Integer burningSecondDegree;

    @Column
    private Integer burningThirdDegree;

    @Column
    private Integer heartRate1;

    @Column
    private Integer heartRate2;

    @Column
    private String bloodPressure1;

    @Column
    private String bloodPressure2;

    public Boolean getStrongBleeding() {
        return strongBleeding;
    }

    public void setStrongBleeding(Boolean strongBleeding) {
        this.strongBleeding = strongBleeding;
    }

    public Boolean getArrhythmia() {
        return arrhythmia;
    }

    public void setArrhythmia(Boolean arrhythmia) {
        this.arrhythmia = arrhythmia;
    }

    public Boolean getShock() {
        return shock;
    }

    public void setShock(Boolean shock) {
        this.shock = shock;
    }

    public Boolean getCardiacArrest() {
        return cardiacArrest;
    }

    public void setCardiacArrest(Boolean cardiacArrest) {
        this.cardiacArrest = cardiacArrest;
    }

    public Boolean getBurning() {
        return burning;
    }

    public void setBurning(Boolean burning) {
        this.burning = burning;
    }

    public Integer getBurningFirstDegree() {
        return burningFirstDegree;
    }

    public void setBurningFirstDegree(Integer burningFirstDegree) {
        this.burningFirstDegree = burningFirstDegree;
    }

    public Integer getBurningSecondDegree() {
        return burningSecondDegree;
    }

    public void setBurningSecondDegree(Integer burningSecondDegree) {
        this.burningSecondDegree = burningSecondDegree;
    }

    public Integer getBurningThirdDegree() {
        return burningThirdDegree;
    }

    public void setBurningThirdDegree(Integer burningThirdDegree) {
        this.burningThirdDegree = burningThirdDegree;
    }

    public Integer getHeartRate1() {
        return heartRate1;
    }

    public void setHeartRate1(Integer heartRate1) {
        this.heartRate1 = heartRate1;
    }

    public Integer getHeartRate2() {
        return heartRate2;
    }

    public void setHeartRate2(Integer heartRate2) {
        this.heartRate2 = heartRate2;
    }

    public String getBloodPressure1() {
        return bloodPressure1;
    }

    public void setBloodPressure1(String bloodPressure1) {
        this.bloodPressure1 = bloodPressure1;
    }

    public String getBloodPressure2() {
        return bloodPressure2;
    }

    public void setBloodPressure2(String bloodPressure2) {
        this.bloodPressure2 = bloodPressure2;
    }
}
