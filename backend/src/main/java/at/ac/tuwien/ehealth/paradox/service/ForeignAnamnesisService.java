package at.ac.tuwien.ehealth.paradox.service;

import at.ac.tuwien.ehealth.paradox.entity.ForeignAnamnesisInfo;

public interface ForeignAnamnesisService {

    ForeignAnamnesisInfo getForeignAnamnesis(Long id);

    ForeignAnamnesisInfo saveForeignAnamnesis(ForeignAnamnesisInfo anamnesis, Long medicalOperationID);
}
