package at.ac.tuwien.ehealth.paradox.service.validator;

import at.ac.tuwien.ehealth.paradox.entity.CirculatoryExamination;

public class CirculatoryExaminationValidator extends Validator<CirculatoryExamination> {

    @Override
    protected void doValidation(CirculatoryExamination object) {
        notNullOrName(object.getStrongBleeding(), "Starke Blutung");
        notNullOrName(object.getArrhythmia(), "Arrhythmie");
        notNullOrName(object.getShock(), "(drohender) Schockzustand");
        notNullOrName(object.getCardiacArrest(), "Atem-/Kreislaufstillstand");
        notNullOrName(object.getBurning(), "Verbrennung/Erfrierung/Verätzung");


        //notNullOrName(object.getHeartRate1(), "HF-1 Messung");
        //notNullOrName(object.getHeartRate2(), "HF-2 Messung");
        //notNullOrName(object.getBloodPressure1(), "RR-1 Messung");
        //notNullOrName(object.getBloodPressure2(), "RR-2 Messung");
    }
}
