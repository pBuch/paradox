package at.ac.tuwien.ehealth.paradox.service;

import at.ac.tuwien.ehealth.paradox.entity.MedicalAidOperationInfo;
import at.ac.tuwien.ehealth.paradox.exception.BusinessValidationException;

import java.util.List;

public interface MedicalAidOperationService {
    List<MedicalAidOperationInfo> getTodaysOperations(boolean finished);

    MedicalAidOperationInfo getById(Long id);

    MedicalAidOperationInfo saveOperation(MedicalAidOperationInfo operation) throws BusinessValidationException;
}
