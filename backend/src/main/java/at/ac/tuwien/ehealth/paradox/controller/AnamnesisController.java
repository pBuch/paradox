package at.ac.tuwien.ehealth.paradox.controller;

import at.ac.tuwien.ehealth.paradox.api.AnamnesisApi;
import at.ac.tuwien.ehealth.paradox.controller.mapper.ForeignAnamnesisInfoMapper;
import at.ac.tuwien.ehealth.paradox.dto.ForeignAnamnesisInfoDTO;
import at.ac.tuwien.ehealth.paradox.entity.ForeignAnamnesisInfo;
import at.ac.tuwien.ehealth.paradox.service.ForeignAnamnesisService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
public class AnamnesisController implements AnamnesisApi {

    private final ForeignAnamnesisInfoMapper foreignAnamnesisInfoMapper;
    private final ForeignAnamnesisService foreignAnamnesisService;

    public AnamnesisController(ForeignAnamnesisInfoMapper foreignAnamnesisInfoMapper,
        ForeignAnamnesisService foreignAnamnesisService) {
        this.foreignAnamnesisInfoMapper = foreignAnamnesisInfoMapper;
        this.foreignAnamnesisService = foreignAnamnesisService;
    }

    @Override
    public ResponseEntity<ForeignAnamnesisInfoDTO> getForeignAnamnesis(Long id) {
        ForeignAnamnesisInfoDTO anamnesis =
            foreignAnamnesisInfoMapper.toDto(foreignAnamnesisService.getForeignAnamnesis(id));

        return ResponseEntity.of(Optional.of(anamnesis));
    }

    @Override
    public ResponseEntity<ForeignAnamnesisInfoDTO> saveForeignAnamnesis(
        @Valid ForeignAnamnesisInfoDTO foreignAnamnesisInfoDTO) {
        ForeignAnamnesisInfo savedInfo =
            foreignAnamnesisService.saveForeignAnamnesis(foreignAnamnesisInfoMapper.fromDto(foreignAnamnesisInfoDTO),
                foreignAnamnesisInfoDTO.getMedicalAidOperationInfoID());

        return ResponseEntity.of(Optional.of(foreignAnamnesisInfoMapper.toDto(savedInfo)));
    }
}

