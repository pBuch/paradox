package at.ac.tuwien.ehealth.paradox.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
public class PainLocalisation {

    @Id
    @GeneratedValue
    private Long id;

    @ElementCollection(fetch = FetchType.EAGER)
    private Set<PainIndicator> indicators;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<PainIndicator> getIndicators() {
        return indicators;
    }

    public void setIndicators(Set<PainIndicator> indicators) {
        this.indicators = indicators;
    }
}
