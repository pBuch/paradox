package at.ac.tuwien.ehealth.paradox.entity;

import javax.persistence.*;

@Entity
public class Picture {
    @Id
    @GeneratedValue
    private Long id;

    @Lob
    private String encoded;

    @Column
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEncoded() {
        return encoded;
    }

    public void setEncoded(String encoded) {
        this.encoded = encoded;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
