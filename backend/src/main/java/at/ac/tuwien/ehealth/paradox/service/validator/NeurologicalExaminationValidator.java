package at.ac.tuwien.ehealth.paradox.service.validator;

import at.ac.tuwien.ehealth.paradox.entity.NeurologicalExamination;

public class NeurologicalExaminationValidator extends Validator<NeurologicalExamination> {

    @Override
    protected void doValidation(NeurologicalExamination object) {
        notNullOrName(object.getCramp(), "Krampfgeschehen");
        notNullOrName(object.getSpeakingDisorder(), "Sprachstörung");
        //notNullOrName(object.getPainEstimate(), "Schmerzbeurteilung");

        //notNullOrName(object.getBloodSugar1(), "BZ-1 Messung");
        //notNullOrName(object.getBloodSugar2(), "BZ-2 Messung");
        //notNullOrName(object.getTemp1(), "Temp-1 Messung");
        //notNullOrName(object.getTemp2(), "Temp-2 Messung");
        //notNullOrName(object.getMeasurementTime1(), "Zeitpunkt 1");
        //notNullOrName(object.getMeasurementTime2(), "Zeitpunkt 2");

        notNullOrName(object.getPupilReaction(), "Pupillenreaktion");
        callValidatorOnChild(new PupilReactionValidator(), object.getPupilReaction());
    }
}
