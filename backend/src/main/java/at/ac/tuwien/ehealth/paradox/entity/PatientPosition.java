package at.ac.tuwien.ehealth.paradox.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PatientPosition {

    @Column
    private String consciousness;

    @Column
    private String dangers;

    @Column
    private String foundPosition;

    public String getConsciousness() {
        return consciousness;
    }

    public void setConsciousness(String consciousness) {
        this.consciousness = consciousness;
    }

    public String getDangers() {
        return dangers;
    }

    public void setDangers(String dangers) {
        this.dangers = dangers;
    }

    public String getFoundPosition() {
        return foundPosition;
    }

    public void setFoundPosition(String foundPosition) {
        this.foundPosition = foundPosition;
    }
}
