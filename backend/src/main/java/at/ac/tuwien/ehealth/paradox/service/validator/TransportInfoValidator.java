package at.ac.tuwien.ehealth.paradox.service.validator;

import at.ac.tuwien.ehealth.paradox.entity.TransportInfo;

public class TransportInfoValidator extends Validator<TransportInfo> {

    @Override
    protected void doValidation(TransportInfo object) {
        notNullOrName(object.getKmstart(), "KM Anfang");
        notNullOrName(object.getKmend(), "KM Ende");

        notEmptyOrName(object.getStarttime(), "Beginnzeit");
        notEmptyOrName(object.getEndtime(), "Endzeit");
        notEmptyOrName(object.getPickoff(), "Abholort");
        notEmptyOrName(object.getDestination(), "Zielort");
        notEmptyOrName(object.getSuspectedDiagnosis(), "Verdachtsdiagnose");
    }
}
