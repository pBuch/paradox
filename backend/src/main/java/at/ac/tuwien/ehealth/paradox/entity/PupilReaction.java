package at.ac.tuwien.ehealth.paradox.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PupilReaction {

    @Column
    private Boolean pupilSmallLeft;

    @Column
    private Boolean pupilSmallRight;

    @Column
    private Boolean pupilMediumLeft;

    @Column
    private Boolean pupilMediumRight;

    @Column
    private Boolean pupilLargeLeft;

    @Column
    private Boolean pupilLargeRight;

    @Column
    private Boolean pupilLightUnresponsiveLeft;

    @Column
    private Boolean pupilLightUnresponsiveRight;

    public Boolean getPupilSmallLeft() {
        return pupilSmallLeft;
    }

    public void setPupilSmallLeft(Boolean pupilSmallLeft) {
        this.pupilSmallLeft = pupilSmallLeft;
    }

    public Boolean getPupilSmallRight() {
        return pupilSmallRight;
    }

    public void setPupilSmallRight(Boolean pupilSmallRight) {
        this.pupilSmallRight = pupilSmallRight;
    }

    public Boolean getPupilMediumLeft() {
        return pupilMediumLeft;
    }

    public void setPupilMediumLeft(Boolean pupilMediumLeft) {
        this.pupilMediumLeft = pupilMediumLeft;
    }

    public Boolean getPupilMediumRight() {
        return pupilMediumRight;
    }

    public void setPupilMediumRight(Boolean pupilMediumRight) {
        this.pupilMediumRight = pupilMediumRight;
    }

    public Boolean getPupilLargeLeft() {
        return pupilLargeLeft;
    }

    public void setPupilLargeLeft(Boolean pupilLargeLeft) {
        this.pupilLargeLeft = pupilLargeLeft;
    }

    public Boolean getPupilLargeRight() {
        return pupilLargeRight;
    }

    public void setPupilLargeRight(Boolean pupilLargeRight) {
        this.pupilLargeRight = pupilLargeRight;
    }

    public Boolean getPupilLightUnresponsiveLeft() {
        return pupilLightUnresponsiveLeft;
    }

    public void setPupilLightUnresponsiveLeft(Boolean pupilLightUnresponsiveLeft) {
        this.pupilLightUnresponsiveLeft = pupilLightUnresponsiveLeft;
    }

    public Boolean getPupilLightUnresponsiveRight() {
        return pupilLightUnresponsiveRight;
    }

    public void setPupilLightUnresponsiveRight(Boolean pupilLightUnresponsiveRight) {
        this.pupilLightUnresponsiveRight = pupilLightUnresponsiveRight;
    }
}
