package at.ac.tuwien.ehealth.paradox.service.impl;

import at.ac.tuwien.ehealth.paradox.dto.OperationState;
import at.ac.tuwien.ehealth.paradox.entity.MedicalAidOperationInfo;
import at.ac.tuwien.ehealth.paradox.exception.BusinessValidationException;
import at.ac.tuwien.ehealth.paradox.repository.MedicalAidOperationInfoRepository;
import at.ac.tuwien.ehealth.paradox.service.MedicalAidOperationService;
import at.ac.tuwien.ehealth.paradox.service.validator.MedicalAidOperationInfoValidator;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class MedicalAidOperationServiceImpl implements MedicalAidOperationService {

    private final MedicalAidOperationInfoRepository medicalAidOperationInfoRepository;

    public MedicalAidOperationServiceImpl(MedicalAidOperationInfoRepository medicalAidOperationInfoRepository) {
        this.medicalAidOperationInfoRepository = medicalAidOperationInfoRepository;
    }

    @Override
    public List<MedicalAidOperationInfo> getTodaysOperations(boolean finished) {
        if (finished) {
            return medicalAidOperationInfoRepository.getAllByEndNotNullAndEndAfter(LocalDate.now().atStartOfDay());
        }
        return medicalAidOperationInfoRepository.getAllByEndNull();
    }

    @Override
    public MedicalAidOperationInfo saveOperation(MedicalAidOperationInfo operation) throws BusinessValidationException {
        var operationState = operation.getState();
        if (operationState != null && operationState == OperationState.COMPLETED) {
            new MedicalAidOperationInfoValidator().build(operation).validate();
        }
        return medicalAidOperationInfoRepository.save(operation);
    }

    @Override
    public MedicalAidOperationInfo getById(Long id) {
        return medicalAidOperationInfoRepository.findById(id).orElse(null);
    }
}
