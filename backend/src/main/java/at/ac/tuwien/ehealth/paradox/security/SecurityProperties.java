package at.ac.tuwien.ehealth.paradox.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * This configuration class offers all necessary security properties in an immutable manner
 */
@Configuration
public class SecurityProperties {

    @Autowired
    private Auth auth;

    public List<String> getCorsOrigins() {
        return auth.getCorsOrigins();
    }

    @Bean
    @ConfigurationProperties(prefix = "security.auth")
    protected Auth auth() {
        return new Auth();
    }

    protected class Auth {
        private List<String> corsOrigins;

        public List<String> getCorsOrigins() {
            return corsOrigins;
        }

        public void setCorsOrigins(List<String> corsOrigins) {
            this.corsOrigins = corsOrigins;
        }
    }
}
