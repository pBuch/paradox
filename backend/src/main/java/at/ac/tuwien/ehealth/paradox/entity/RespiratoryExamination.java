package at.ac.tuwien.ehealth.paradox.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class RespiratoryExamination {

    @Column
    private Boolean normalBreathing;

    @Column
    private Boolean dyspnoea;

    @Column
    private Boolean constipation;

    @Column
    private Boolean abnormalSounds;

    @Column
    private Boolean asymmetricalMovements;

    @Column
    private Boolean abnormalBreathing;

    @Column
    private Boolean hyperventilation;

    @Column
    private Integer breathingFrequency1;

    @Column
    private Integer breathingFrequency2;

    @Column
    private Integer oxygenSaturation1;

    @Column
    private Integer oxygenSaturation2;

    public Boolean getNormalBreathing() {
        return normalBreathing;
    }

    public void setNormalBreathing(Boolean normalBreathing) {
        this.normalBreathing = normalBreathing;
    }

    public Boolean getDyspnoea() {
        return dyspnoea;
    }

    public void setDyspnoea(Boolean dyspnoea) {
        this.dyspnoea = dyspnoea;
    }

    public Boolean getConstipation() {
        return constipation;
    }

    public void setConstipation(Boolean constipation) {
        this.constipation = constipation;
    }

    public Boolean getAbnormalSounds() {
        return abnormalSounds;
    }

    public void setAbnormalSounds(Boolean abnormalSounds) {
        this.abnormalSounds = abnormalSounds;
    }

    public Boolean getAsymmetricalMovements() {
        return asymmetricalMovements;
    }

    public void setAsymmetricalMovements(Boolean asymmetricalMovements) {
        this.asymmetricalMovements = asymmetricalMovements;
    }

    public Boolean getAbnormalBreathing() {
        return abnormalBreathing;
    }

    public void setAbnormalBreathing(Boolean abnormalBreathing) {
        this.abnormalBreathing = abnormalBreathing;
    }

    public Boolean getHyperventilation() {
        return hyperventilation;
    }

    public void setHyperventilation(Boolean hyperventilation) {
        this.hyperventilation = hyperventilation;
    }

    public Integer getBreathingFrequency1() {
        return breathingFrequency1;
    }

    public void setBreathingFrequency1(Integer breathingFrequency1) {
        this.breathingFrequency1 = breathingFrequency1;
    }

    public Integer getBreathingFrequency2() {
        return breathingFrequency2;
    }

    public void setBreathingFrequency2(Integer breathingFrequency2) {
        this.breathingFrequency2 = breathingFrequency2;
    }

    public Integer getOxygenSaturation1() {
        return oxygenSaturation1;
    }

    public void setOxygenSaturation1(Integer oxygenSaturation1) {
        this.oxygenSaturation1 = oxygenSaturation1;
    }

    public Integer getOxygenSaturation2() {
        return oxygenSaturation2;
    }

    public void setOxygenSaturation2(Integer oxygenSaturation2) {
        this.oxygenSaturation2 = oxygenSaturation2;
    }
}
