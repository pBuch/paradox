package at.ac.tuwien.ehealth.paradox.entity;

import javax.persistence.*;

@Entity
public class ContactInfoAuthor {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;

    @Column
    private String relation;

    @Column
    private String telnr;

    @Column
    private boolean stationary;

    @Column
    private boolean released;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getTelnr() {
        return telnr;
    }

    public void setTelnr(String telnr) {
        this.telnr = telnr;
    }

    public boolean getStationary() {
        return stationary;
    }

    public void setStationary(boolean stationary) {
        this.stationary = stationary;
    }

    public boolean getReleased() {
        return released;
    }

    public void setReleased(boolean released) {
        this.released = released;
    }
}
