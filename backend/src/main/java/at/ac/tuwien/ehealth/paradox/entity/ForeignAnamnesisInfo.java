package at.ac.tuwien.ehealth.paradox.entity;

import at.ac.tuwien.ehealth.paradox.dto.OperationState;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
public class ForeignAnamnesisInfo {

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne
    private MedicalAidOperationInfo medicalAidOperationInfo;

    @OneToOne(cascade = CascadeType.ALL)
    private ContactInfoAuthor contactInfoAuthor;

    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "foreignAnamnesisInfo", fetch = FetchType.EAGER)
    //private Set<OtherContactInfo> otherContactInfos;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Picture medsPicture;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Picture decreePicture;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Picture otherPicture;

    @Column(columnDefinition = "TEXT")
    private String otherText;

    @OneToOne(cascade = CascadeType.ALL)
    private MedicalInfoPatient medicalInfoPatient;

    @OneToMany(cascade = CascadeType.ALL,
               mappedBy = "foreignAnamnesisInfo",
               fetch = FetchType.EAGER,
               orphanRemoval = true)
    private Set<OtherContactInfo> otherContactInfos;

    @Column
    private LocalDateTime start;

    @Column
    private LocalDateTime end;

    @Enumerated
    private OperationState state;

    public MedicalInfoPatient getMedicalInfoPatient() {
        return medicalInfoPatient;
    }

    public void setMedicalInfoPatient(MedicalInfoPatient medicalInfoPatient) {
        this.medicalInfoPatient = medicalInfoPatient;
    }

    public MedicalAidOperationInfo getMedicalAidOperationInfo() {
        return medicalAidOperationInfo;
    }

    public void setMedicalAidOperationInfo(MedicalAidOperationInfo medicalAidOperationInfo) {
        this.medicalAidOperationInfo = medicalAidOperationInfo;
    }

    public Set<OtherContactInfo> getOtherContactInfos() {
        return otherContactInfos;
    }

    public void setOtherContactInfos(Set<OtherContactInfo> otherContactInfos) {
        this.otherContactInfos = otherContactInfos;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ContactInfoAuthor getContactInfoAuthor() {
        return contactInfoAuthor;
    }

    public void setContactInfoAuthor(ContactInfoAuthor contactInfoAuthor) {
        this.contactInfoAuthor = contactInfoAuthor;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    public OperationState getState() {
        return state;
    }

    public void setState(OperationState state) {
        this.state = state;
    }

    public Picture getMedsPicture() {
        return medsPicture;
    }

    public void setMedsPicture(Picture medsImage) {
        this.medsPicture = medsImage;
    }

    public Picture getDecreePicture() {
        return decreePicture;
    }

    public void setDecreePicture(Picture decreePicture) {
        this.decreePicture = decreePicture;
    }

    public Picture getOtherPicture() {
        return otherPicture;
    }

    public void setOtherPicture(Picture otherPicture) {
        this.otherPicture = otherPicture;
    }

    public String getOtherText() {
        return otherText;
    }

    public void setOtherText(String otherText) {
        this.otherText = otherText;
    }
}


