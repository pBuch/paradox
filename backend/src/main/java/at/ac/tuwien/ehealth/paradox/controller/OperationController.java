package at.ac.tuwien.ehealth.paradox.controller;

import at.ac.tuwien.ehealth.paradox.api.OperationApi;
import at.ac.tuwien.ehealth.paradox.controller.mapper.MedicalAidOperationInfoMapper;
import at.ac.tuwien.ehealth.paradox.dto.MedicalAidOperationInfoDTO;
import at.ac.tuwien.ehealth.paradox.entity.MedicalAidOperationInfo;
import at.ac.tuwien.ehealth.paradox.service.MedicalAidOperationService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
public class OperationController implements OperationApi {

    private final MedicalAidOperationInfoMapper medicalAidOperationInfoMapper;
    private final MedicalAidOperationService medicalAidOperationService;

    public OperationController(MedicalAidOperationInfoMapper medicalAidOperationInfoMapper,
        MedicalAidOperationService medicalAidOperationService) {
        this.medicalAidOperationInfoMapper = medicalAidOperationInfoMapper;
        this.medicalAidOperationService = medicalAidOperationService;
    }

    @Override
    public ResponseEntity<List<MedicalAidOperationInfoDTO>> getTodaysOperations(Optional<Boolean> finished) {
        List<MedicalAidOperationInfoDTO> operations =
            medicalAidOperationInfoMapper.toDto(medicalAidOperationService.getTodaysOperations(finished.orElse(false)));

        return ResponseEntity.of(Optional.of(operations));
    }

    @Override
    public ResponseEntity<MedicalAidOperationInfoDTO> saveOperation(
        @Valid MedicalAidOperationInfoDTO medicalAidOperationInfoDTO) {
        MedicalAidOperationInfo savedInfo =
            medicalAidOperationService.saveOperation(medicalAidOperationInfoMapper.fromDto(medicalAidOperationInfoDTO));

        return ResponseEntity.of(Optional.of(medicalAidOperationInfoMapper.toDto(savedInfo)));
    }

    @Override
    public ResponseEntity<MedicalAidOperationInfoDTO> getOperationById(Long id) {
        return ResponseEntity.ok(medicalAidOperationInfoMapper.toDto(medicalAidOperationService.getById(id)));
    }
}
