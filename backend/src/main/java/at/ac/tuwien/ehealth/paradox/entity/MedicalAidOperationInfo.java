package at.ac.tuwien.ehealth.paradox.entity;

import at.ac.tuwien.ehealth.paradox.dto.OperationState;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class MedicalAidOperationInfo {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private LocalDateTime start;

    @Column
    private LocalDateTime end;

    @Enumerated
    private OperationState state;

    @OneToOne(cascade = CascadeType.ALL)
    private BasicInfo basicInfo;

    @OneToOne(cascade = CascadeType.ALL)
    private Examination examination;

    @OneToOne(cascade = CascadeType.ALL)
    private PainLocalisation painLocalisation;

    @OneToOne(mappedBy = "medicalAidOperationInfo")
    private ForeignAnamnesisInfo foreignAnamnesisInfo;


    public ForeignAnamnesisInfo getForeignAnamnesisInfo() {
        return foreignAnamnesisInfo;
    }

    public void setForeignAnamnesisInfo(ForeignAnamnesisInfo foreignAnamnesisInfo) {
        this.foreignAnamnesisInfo = foreignAnamnesisInfo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    public OperationState getState() {
        return state;
    }

    public void setState(OperationState state) {
        this.state = state;
    }

    public BasicInfo getBasicInfo() {
        return basicInfo;
    }

    public void setBasicInfo(BasicInfo basicInfo) {
        this.basicInfo = basicInfo;
    }

    public Examination getExamination() {
        return examination;
    }

    public void setExamination(Examination examination) {
        this.examination = examination;
    }

    public PainLocalisation getPainLocalisation() {
        return painLocalisation;
    }

    public void setPainLocalisation(PainLocalisation painLocalisation) {
        this.painLocalisation = painLocalisation;
    }
}
