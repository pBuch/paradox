package at.ac.tuwien.ehealth.paradox.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

@Embeddable
public class PersonalInfo {

    @Column
    private String name;

    @Column
    private String svnr;

    @Column
    private String telnr;

    @Column
    private String svCarrier;

    @Embedded
    private Address address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSvnr() {
        return svnr;
    }

    public void setSvnr(String svnr) {
        this.svnr = svnr;
    }

    public String getTelnr() {
        return telnr;
    }

    public void setTelnr(String telnr) {
        this.telnr = telnr;
    }

    public String getSvCarrier() {
        return svCarrier;
    }

    public void setSvCarrier(String svCarrier) {
        this.svCarrier = svCarrier;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
