package at.ac.tuwien.ehealth.paradox.service.validator;

import at.ac.tuwien.ehealth.paradox.entity.MedicalAidOperationInfo;

public class MedicalAidOperationInfoValidator extends Validator<MedicalAidOperationInfo> {

    @Override
    protected void doValidation(MedicalAidOperationInfo object) {
        notNullOrName(object.getStart(), "Startzeit");
        notNullOrName(object.getEnd(), "Endzeit");
        notNullOrName(object.getState(), "Status");

        notNullOrName(object.getBasicInfo(), "Allgemeine Information");
        callValidatorOnChild(new BasicInfoValidator(), object.getBasicInfo());

        notNullOrName(object.getExamination(), "Behandlung");
        callValidatorOnChild(new ExaminationValidator(), object.getExamination());
    }
}
