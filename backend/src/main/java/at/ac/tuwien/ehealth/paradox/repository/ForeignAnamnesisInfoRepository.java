package at.ac.tuwien.ehealth.paradox.repository;

import at.ac.tuwien.ehealth.paradox.entity.ForeignAnamnesisInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ForeignAnamnesisInfoRepository extends JpaRepository<ForeignAnamnesisInfo, Long> {
}
