package at.ac.tuwien.ehealth.paradox.service.validator;

import at.ac.tuwien.ehealth.paradox.entity.Examination;

public class ExaminationValidator extends Validator<Examination> {

    @Override
    protected void doValidation(Examination object) {
        notNullOrName(object.getPatientPosition(), "Patienten-/Lagebeurteilung");
        callValidatorOnChild(new PatientPositionValidator(), object.getPatientPosition());

        notNullOrName(object.getRespiratoryExamination(), "Atemwege, Atmung");
        callValidatorOnChild(new RespiratoryExaminationValidator(), object.getRespiratoryExamination());

        notNullOrName(object.getCirculatoryExamination(), "Kreislauf");
        callValidatorOnChild(new CirculatoryExaminationValidator(), object.getCirculatoryExamination());

        notNullOrName(object.getPatientCritical(), "Notarztindikation");

        notNullOrName(object.getNeurologicalExamination(), "Neurologie");
        callValidatorOnChild(new NeurologicalExaminationValidator(), object.getNeurologicalExamination());

        notNullOrName(object.getFurtherExamination(), "Erweiterte Untersuchung");
        callValidatorOnChild(new FurtherExaminationValidator(), object.getFurtherExamination());
    }
}
