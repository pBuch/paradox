package at.ac.tuwien.ehealth.paradox.service.validator;

import at.ac.tuwien.ehealth.paradox.entity.PatientPosition;

public class PatientPositionValidator extends Validator<PatientPosition> {

    @Override
    protected void doValidation(PatientPosition object) {
        notNullOrName(object.getConsciousness(), "Bewusstseinslage");
        notNullOrName(object.getFoundPosition(), "Vorgefundene Lage");
        notNullOrName(object.getDangers(), "Gefahrenzone");
    }
}
