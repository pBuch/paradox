package at.ac.tuwien.ehealth.paradox.service.validator;

import at.ac.tuwien.ehealth.paradox.entity.PupilReaction;

public class PupilReactionValidator extends Validator<PupilReaction> {

    @Override
    protected void doValidation(PupilReaction object) {
        notNullOrName(object.getPupilSmallLeft(), "Pupillenreaktion: links - eng");
        notNullOrName(object.getPupilSmallRight(), "Pupillenreaktion: rechts - eng");
        notNullOrName(object.getPupilMediumLeft(), "Pupillenreaktion: links - mittel");
        notNullOrName(object.getPupilMediumRight(), "Pupillenreaktion: rechts - mittel");
        notNullOrName(object.getPupilLargeLeft(), "Pupillenreaktion: links - weit");
        notNullOrName(object.getPupilLargeRight(), "Pupillenreaktion: rechts - weit");
        notNullOrName(object.getPupilLightUnresponsiveLeft(), "Pupillenreaktion: links - lichtstarr");
        notNullOrName(object.getPupilLightUnresponsiveRight(), "Pupillenreaktion: rechts - lichtstarr");
    }
}
