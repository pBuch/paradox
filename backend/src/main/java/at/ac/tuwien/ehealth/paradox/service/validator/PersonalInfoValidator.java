package at.ac.tuwien.ehealth.paradox.service.validator;

import at.ac.tuwien.ehealth.paradox.entity.PersonalInfo;

public class PersonalInfoValidator extends Validator<PersonalInfo> {

    @Override
    protected void doValidation(PersonalInfo object) {
        notNullOrName(object.getName(), "Name");
        notNullOrName(object.getSvnr(), "Sozialversicherungsnummer");
        notNullOrName(object.getSvCarrier(), "Sozialversicherungsträger");

        notEmptyOrName(object.getName(), "Name");
        notEmptyOrName(object.getSvnr(), "Sozialversicherungsnummer");
        notEmptyOrName(object.getSvCarrier(), "Sozialversicherungsträger");

        notNullOrName(object.getAddress(), "Adresse");
        callValidatorOnChild(new AddressValidator(), object.getAddress());
    }
}
