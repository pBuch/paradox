package at.ac.tuwien.ehealth.paradox.entity;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import java.util.Set;

@Embeddable
public class FurtherExamination {

    @ElementCollection(fetch = FetchType.EAGER)
    private Set<String> allergies;

    @ElementCollection(fetch = FetchType.EAGER)
    private Set<String> meds;

    @Column
    private String patientHistory;

    @Column
    private String beforeTheEvent;

    public Set<String> getAllergies() {
        return allergies;
    }

    public void setAllergies(Set<String> allergies) {
        this.allergies = allergies;
    }

    public Set<String> getMeds() {
        return meds;
    }

    public void setMeds(Set<String> meds) {
        this.meds = meds;
    }

    public String getPatientHistory() {
        return patientHistory;
    }

    public void setPatientHistory(String patientHistory) {
        this.patientHistory = patientHistory;
    }

    public String getBeforeTheEvent() {
        return beforeTheEvent;
    }

    public void setBeforeTheEvent(String beforeTheEvent) {
        this.beforeTheEvent = beforeTheEvent;
    }
}
