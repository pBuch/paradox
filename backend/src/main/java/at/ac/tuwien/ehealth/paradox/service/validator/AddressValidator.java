package at.ac.tuwien.ehealth.paradox.service.validator;

import at.ac.tuwien.ehealth.paradox.entity.Address;

public class AddressValidator extends Validator<Address>{

    @Override
    protected void doValidation(Address object) {
        notEmptyOrName(object.getStreet(), "Straße");
        notEmptyOrName(object.getHousenr(), "Hausnummer");
        notEmptyOrName(object.getCity(), "Ort");
        notEmptyOrName(object.getPostalcode(), "Postleitzahl");
        notEmptyOrName(object.getCountry(), "Staat");
    }
}
