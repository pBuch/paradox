package at.ac.tuwien.ehealth.paradox.service.validator;

import at.ac.tuwien.ehealth.paradox.entity.BasicInfo;

public class BasicInfoValidator extends Validator<BasicInfo> {

    @Override
    protected void doValidation(BasicInfo object) {
        notNullOrName(object.getPersonalInfo(), "Personendaten");
        callValidatorOnChild(new PersonalInfoValidator(), object.getPersonalInfo());
        notNullOrName(object.getTransportInfo(), "Transportdaten");
        callValidatorOnChild(new TransportInfoValidator(), object.getTransportInfo());
    }

}
