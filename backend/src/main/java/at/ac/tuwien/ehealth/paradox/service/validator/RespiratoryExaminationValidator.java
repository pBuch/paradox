package at.ac.tuwien.ehealth.paradox.service.validator;

import at.ac.tuwien.ehealth.paradox.entity.RespiratoryExamination;

public class RespiratoryExaminationValidator extends Validator<RespiratoryExamination> {

    @Override
    protected void doValidation(RespiratoryExamination object) {
        notNullOrName(object.getNormalBreathing(), "Unauffällig");
        notNullOrName(object.getDyspnoea(), "Atembeschwerden/Zyanose");
        notNullOrName(object.getConstipation(), "Atemwegsverlegung");
        notNullOrName(object.getAbnormalSounds(), "Abnormale Atemgeräusche");
        notNullOrName(object.getAsymmetricalMovements(), "Asymmetrische Brustkorbbewegung");
        notNullOrName(object.getAbnormalBreathing(), "Keine (normale) Atmung");
        notNullOrName(object.getHyperventilation(), "Hyperventilation");
    }
}
