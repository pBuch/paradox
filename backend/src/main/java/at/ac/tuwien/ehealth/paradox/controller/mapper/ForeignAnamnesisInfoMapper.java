package at.ac.tuwien.ehealth.paradox.controller.mapper;

import at.ac.tuwien.ehealth.paradox.dto.AddressDTO;
import at.ac.tuwien.ehealth.paradox.dto.ForeignAnamnesisInfoDTO;
import at.ac.tuwien.ehealth.paradox.dto.MedicalAidOperationInfoDTO;
import at.ac.tuwien.ehealth.paradox.dto.PupilReactionDTO;
import at.ac.tuwien.ehealth.paradox.entity.ForeignAnamnesisInfo;
import at.ac.tuwien.ehealth.paradox.entity.MedicalAidOperationInfo;
import at.ac.tuwien.ehealth.paradox.entity.OtherContactInfo;
import org.mapstruct.*;

import java.util.List;

@Mapper(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
public interface ForeignAnamnesisInfoMapper {

    @Mapping(target="otherContactInfos", source = "contacts")
    ForeignAnamnesisInfo fromDto(ForeignAnamnesisInfoDTO dto);

    @Mapping(target = "contacts", source = "otherContactInfos")
    @Mapping(target = "medicalAidOperationInfoID", source = "medicalAidOperationInfo.id")
    ForeignAnamnesisInfoDTO toDto(ForeignAnamnesisInfo entity);

    List<ForeignAnamnesisInfoDTO> toDto(List<ForeignAnamnesisInfo> entities);
    }
