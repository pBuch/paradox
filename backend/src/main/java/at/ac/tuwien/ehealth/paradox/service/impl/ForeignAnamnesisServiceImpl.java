package at.ac.tuwien.ehealth.paradox.service.impl;

import at.ac.tuwien.ehealth.paradox.entity.ForeignAnamnesisInfo;
import at.ac.tuwien.ehealth.paradox.entity.MedicalAidOperationInfo;
import at.ac.tuwien.ehealth.paradox.entity.OtherContactInfo;
import at.ac.tuwien.ehealth.paradox.repository.ForeignAnamnesisInfoRepository;
import at.ac.tuwien.ehealth.paradox.service.ForeignAnamnesisService;
import at.ac.tuwien.ehealth.paradox.service.MedicalAidOperationService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ForeignAnamnesisServiceImpl implements ForeignAnamnesisService {

    private final ForeignAnamnesisInfoRepository foreignAnamnesisInfoRepository;
    private final MedicalAidOperationService medicalAidOperationService;

    public ForeignAnamnesisServiceImpl(ForeignAnamnesisInfoRepository foreignAnamnesisInfoRepository,
        MedicalAidOperationService medicalAidOperationService) {
        this.foreignAnamnesisInfoRepository = foreignAnamnesisInfoRepository;
        this.medicalAidOperationService = medicalAidOperationService;
    }

    @Override
    public ForeignAnamnesisInfo getForeignAnamnesis(Long id) {
        return foreignAnamnesisInfoRepository.findById(id)
            .orElseThrow(() -> new IllegalArgumentException("Didn't find foreign anamnesis with this id"));
    }

    @Override
    public ForeignAnamnesisInfo saveForeignAnamnesis(ForeignAnamnesisInfo anamnesis, Long medicalOperationID) {
        MedicalAidOperationInfo medicalInfo = medicalAidOperationService.getById(medicalOperationID);
        anamnesis.setMedicalAidOperationInfo(medicalInfo);
        anamnesis.getOtherContactInfos().forEach(contact -> contact.setForeignAnamnesisInfo(anamnesis));
        return foreignAnamnesisInfoRepository.save(anamnesis);
    }


}
