package at.ac.tuwien.ehealth.paradox.repository;

import at.ac.tuwien.ehealth.paradox.entity.MedicalAidOperationInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface MedicalAidOperationInfoRepository extends JpaRepository<MedicalAidOperationInfo, Long> {

    List<MedicalAidOperationInfo> getAllByEndNotNullAndEndAfter(LocalDateTime today);

    List<MedicalAidOperationInfo> getAllByEndNull();
}
