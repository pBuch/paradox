package at.ac.tuwien.ehealth.paradox.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TransportInfo {

    @Column
    private String starttime;

    @Column
    private Long kmstart;

    @Column
    private String pickoff;

    @Column
    private String suspectedDiagnosis;

    @Column
    private String endtime;

    @Column
    private Long kmend;

    @Column
    private String destination;

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public Long getKmstart() {
        return kmstart;
    }

    public void setKmstart(Long kmstart) {
        this.kmstart = kmstart;
    }

    public String getPickoff() {
        return pickoff;
    }

    public void setPickoff(String pickoff) {
        this.pickoff = pickoff;
    }

    public String getSuspectedDiagnosis() {
        return suspectedDiagnosis;
    }

    public void setSuspectedDiagnosis(String suspectedDiagnosis) {
        this.suspectedDiagnosis = suspectedDiagnosis;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public Long getKmend() {
        return kmend;
    }

    public void setKmend(Long kmend) {
        this.kmend = kmend;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }
}
