package at.ac.tuwien.ehealth.paradox.controller.mapper;

import at.ac.tuwien.ehealth.paradox.dto.AddressDTO;
import at.ac.tuwien.ehealth.paradox.dto.MedicalAidOperationInfoDTO;
import at.ac.tuwien.ehealth.paradox.dto.PupilReactionDTO;
import at.ac.tuwien.ehealth.paradox.entity.MedicalAidOperationInfo;
import org.mapstruct.*;

import java.util.List;

@Mapper(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
public interface MedicalAidOperationInfoMapper {

    MedicalAidOperationInfo fromDto(MedicalAidOperationInfoDTO dto);

    @Mapping(target="foreignAnamnesisID", source="foreignAnamnesisInfo.id")
    MedicalAidOperationInfoDTO toDto(MedicalAidOperationInfo entity);

    List<MedicalAidOperationInfoDTO> toDto(List<MedicalAidOperationInfo> entities);

    @AfterMapping
    default void insertEmptyAddress(MedicalAidOperationInfo entity,
        @MappingTarget MedicalAidOperationInfoDTO medicalAidOperationInfoDTO) {

        if (medicalAidOperationInfoDTO.getBasicInfo().getPersonalInfo().getAddress() == null) {
            medicalAidOperationInfoDTO.getBasicInfo().getPersonalInfo().setAddress(new AddressDTO());
        }
        if (medicalAidOperationInfoDTO.getExamination().getNeurologicalExamination().getPupilReaction() == null) {
            medicalAidOperationInfoDTO.getExamination()
                .getNeurologicalExamination()
                .setPupilReaction(new PupilReactionDTO());
        }
    }
}
