package at.ac.tuwien.ehealth.paradox.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.math.BigDecimal;

@Embeddable
public class NeurologicalExamination {
    @Column
    private Boolean cramp;

    @Column
    private Boolean speakingDisorder;

    @Column
    private String painEstimate;

    @Embedded
    private PupilReaction pupilReaction;

    @Column
    private Integer bloodSugar1;

    @Column
    private Integer bloodSugar2;

    @Column
    private BigDecimal temp1;

    @Column
    private BigDecimal temp2;

    @Column
    private String measurementTime1;

    @Column
    private String measurementTime2;

    public Boolean getCramp() {
        return cramp;
    }

    public void setCramp(Boolean cramp) {
        this.cramp = cramp;
    }

    public Boolean getSpeakingDisorder() {
        return speakingDisorder;
    }

    public void setSpeakingDisorder(Boolean speakingDisorder) {
        this.speakingDisorder = speakingDisorder;
    }

    public String getPainEstimate() {
        return painEstimate;
    }

    public void setPainEstimate(String painEstimate) {
        this.painEstimate = painEstimate;
    }

    public PupilReaction getPupilReaction() {
        return pupilReaction;
    }

    public void setPupilReaction(PupilReaction pupilReaction) {
        this.pupilReaction = pupilReaction;
    }

    public Integer getBloodSugar1() {
        return bloodSugar1;
    }

    public void setBloodSugar1(Integer bloodSugar1) {
        this.bloodSugar1 = bloodSugar1;
    }

    public Integer getBloodSugar2() {
        return bloodSugar2;
    }

    public void setBloodSugar2(Integer bloodSugar2) {
        this.bloodSugar2 = bloodSugar2;
    }

    public BigDecimal getTemp1() {
        return temp1;
    }

    public void setTemp1(BigDecimal temp1) {
        this.temp1 = temp1;
    }

    public BigDecimal getTemp2() {
        return temp2;
    }

    public void setTemp2(BigDecimal temp2) {
        this.temp2 = temp2;
    }

    public String getMeasurementTime1() {
        return measurementTime1;
    }

    public void setMeasurementTime1(String measurementTime1) {
        this.measurementTime1 = measurementTime1;
    }

    public String getMeasurementTime2() {
        return measurementTime2;
    }

    public void setMeasurementTime2(String measurementTime2) {
        this.measurementTime2 = measurementTime2;
    }
}
