package at.ac.tuwien.ehealth.paradox.service.validator;

import at.ac.tuwien.ehealth.paradox.entity.FurtherExamination;

public class FurtherExaminationValidator extends Validator<FurtherExamination> {

    @Override
    protected void doValidation(FurtherExamination object) {
        notNullOrName(object.getAllergies(), "Allergien");
        notNullOrName(object.getMeds(), "Medikamente");
        //notNullOrName(object.getPatientHistory(), "Patientengeschichte");
        //notNullOrName(object.getBeforeTheEvent(), "Ereignisse vor dem Geschehen");
    }
}
