openapi: 3.0.1
info:
  title: Ticketline
  description: This is the definition of the WebApi of Ticketline - as planned and
    implemented by Group 16 SE&PM SS2020
  contact:
    email: e11808229@student.tuwien.ac.at
  version: 0.0.1
servers:
  - url: http://localhost:8080/
tags:
  - name: operations
    description: Operation access
  - name: anamnesis
    description: ForeignAnamnesis access
paths:
  /anamnesis/{id}:
    get:
      tags:
        - anamnesis
      summary: Get the current anamnesis
      operationId: getForeignAnamnesis
      parameters:
        - in: path
          name: id
          required: true
          schema:
            type: integer
            format: int64
          description: get the foreign anamnesis of the provided id
          example: 57
      responses:
        200:
          description: show the anamnesis
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ForeignAnamnesisInfoDTO'
  /anamnesis:
    post:
      tags:
        - anamnesis
      summary: save an anamnesis or update an existing one
      operationId: saveForeignAnamnesis
      requestBody:
        description: Anamnesis to update or create
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ForeignAnamnesisInfoDTO'
        required: true
      responses:
        201:
          description: Successfully created a new anamnesis entry
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ForeignAnamnesisInfoDTO'
        200:
          description: Successfully updated an existing anamnesis entry
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ForeignAnamnesisInfoDTO'
        400:
          description: Error while parsing the request and validating it
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorDTO'
  /operation/{id}:
    get:
      tags:
        - operations
      summary: Get the operation by id
      operationId: getOperationById
      parameters:
        - in: path
          name: id
          required: true
          schema:
            type: integer
            format: int64
          description: get the operation of the provided id
          example: 57
      responses:
        200:
          description: show the operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MedicalAidOperationInfoDTO'
  /operation:
    get:
      tags:
        - operations
      summary: Get all operations of the current day
      operationId: getTodaysOperations
      parameters:
        - in: query
          name: finished
          required: false
          schema:
            type: boolean
          description: show only finished or only unfinished operations
          example: true
      responses:
        200:
          description: List of todays operations
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/MedicalAidOperationInfoDTO'
    post:
      tags:
        - operations
      summary: Save a new operation or update an existing one
      operationId: saveOperation
      requestBody:
        description: Operation to update or create
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/MedicalAidOperationInfoDTO'
        required: true
      responses:
        201:
          description: Successfully created a new operation entry
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MedicalAidOperationInfoDTO'
        200:
          description: Successfully updated an existing operation entry
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MedicalAidOperationInfoDTO'
        400:
          description: Error while parsing the request and validating it
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorDTO'

components:
  schemas:
    ForeignAnamnesisInfoDTO:
      type: object
      properties:
        id:
          type: integer
          format: int64
          example: 4
        start:
          type: string
          format: date-time
          example: 2018-06-01T07:30+08:00
        end:
          type: string
          format: date-time
          example: 2018-06-01T07:30+08:00
        state:
          $ref: '#/components/schemas/AnamnesisState'
        contactInfoAuthor:
          $ref: '#/components/schemas/ContactInfoAuthorDTO'
        contacts:
          $ref: '#/components/schemas/ContactsDTO'
        medicalInfoPatient:
          $ref: '#/components/schemas/MedicalInfoPatientDTO'
        medicalAidOperationInfoID:
          type: integer
          format: int64
          example: 32
        medsPicture:
          $ref: '#/components/schemas/PictureDTO'
        decreePicture:
          $ref: '#/components/schemas/PictureDTO'
        otherPicture:
          $ref: '#/components/schemas/PictureDTO'
        otherText:
          type: string
      required:
        - contactInfoAuthor
        - contacts
        - medsPicture
        - decreePicture
        - otherPicture
    AnamnesisState:
      type: string
      enum: [ COMPLETED, IN_PROGRESS ]
      example: COMPLETED
    MedicalInfoPatientDTO:
      type: object
      properties:
        id:
          type: integer
          format: int64
          example: 32
        doctor:
          type: string
          example: Dr.Gregory House
        specialInsuranceName:
          type: string
          example: Uniqum AG
        specialInsurancePolicy:
          type: string
          example: Pl-31415
    ContactsDTO:
      type: array
      items:
        $ref: '#/components/schemas/OtherContactInfoDTO'
      default: []
    OtherContactInfoDTO:
      type: object
      properties:
        id:
          type: integer
          format: int64
          example: 32
        name:
          type: string
          example: Max Mustermann
        relation:
          type: string
          example: Vater
        telnr:
          type: string
          example: 0669 3668550
        stationary:
          type: boolean
          example: true
        released:
          type: boolean
          example: true
    ContactInfoAuthorDTO:
      type: object
      properties:
        id:
          type: integer
          format: int64
          example: 32
        name:
          type: string
          example: Max Mustermann
        relation:
          type: string
          example: Vater
        telnr:
          type: string
          example: 0669 3668550
        stationary:
          type: boolean
          example: true
        released:
          type: boolean
          example: true
    MedicalAidOperationInfoDTO:
      type: object
      properties:
        id:
          type: integer
          format: int64
          example: 3
        start:
          type: string
          format: date-time
          example: 2018-06-01T07:30+08:00
        end:
          type: string
          format: date-time
          example: 2018-06-01T07:30+08:00
        state:
          $ref: '#/components/schemas/OperationState'
        basicInfo:
          $ref: '#/components/schemas/BasicInfoDTO'
        examination:
          $ref: '#/components/schemas/ExminationDTO'
        painLocalisation:
          $ref: '#/components/schemas/PainLocalisationDTO'
        foreignAnamnesisID:
          type: integer
          format: int64
          example: 32
      required:
        - start
        - state
        - basicInfo
        - examination
        - painLocalisation
    OperationState:
      type: string
      enum: [ COMPLETED, IN_PROGRESS ]
      example: COMPLETED
    BasicInfoDTO:
      type: object
      properties:
        id:
          type: integer
          format: int64
          example: 21
        personalInfo:
          $ref: '#/components/schemas/PersonalInfoDTO'
        transportInfo:
          $ref: '#/components/schemas/TransportInfoDTO'
      required:
        - personalInfo
        - transportInfo
    ExminationDTO:
      type: object
      properties:
        id:
          type: integer
          format: int64
          example: 63
        patientPosition:
          $ref: '#/components/schemas/PatientPositionDTO'
        respiratoryExamination:
          $ref: '#/components/schemas/RespiratoryExaminationDTO'
        circulatoryExamination:
          $ref: '#/components/schemas/CirculatoryExaminationDTO'
        patientCritical:
          type: boolean
          example: false
          default: false
        neurologicalExamination:
          $ref: '#/components/schemas/NeurologicalExaminationDTO'
        furtherExamination:
          $ref: '#/components/schemas/FurtherExaminationDTO'
      required:
        - patientPosition
        - respiratoryExamination
        - circulatoryExamination
        - neurologicalExamination
        - furtherExamination
    PainLocalisationDTO:
      type: object
      properties:
        id:
          type: integer
          format: int64
          example: 63
        indicators:
          type: array
          items:
            $ref: '#/components/schemas/PainIndicatorDTO'
          default: []
      required:
        - indicators
    PainIndicatorDTO:
      type: object
      properties:
        description:
          type: string
          example: 'Faustgroße Schwellung'
        shape:
          type: string
          example: '{shape json}'
    PersonalInfoDTO:
      type: object
      properties:
        name:
          type: string
          example: Max Mustermann
          default: ''
        svnr:
          type: string
          example: 9448 19041992
          default: ''
        telnr:
          type: string
          example: +436804988291
          default: ''
        svCarrier:
          type: string
          example: VGKK
          default: ''
        address:
          $ref: '#/components/schemas/AddressDTO'
      required:
        - address
    TransportInfoDTO:
      type: object
      properties:
        starttime:
          type: string
          example: 07:30
          default: ''
        kmstart:
          type: integer
          format: int64
          example: 183243
        pickoff:
          type: string
          example: Dorfstraße 23b, 2340 Mödling
          default: ''
        suspectedDiagnosis:
          type: string
          example: Exikkose
          default: ''
        endtime:
          type: string
          example: 08:43
          default: ''
        kmend:
          type: integer
          format: int64
          example: 183256
        destination:
          type: string
          example: KH Baden
          default: ''
    PatientPositionDTO:
      type: object
      properties:
        consciousness:
          type: string
          example: orientiert
        dangers:
          type: string
          example: CO-Alarm
        foundPosition:
          type: string
          example: sitzend
    RespiratoryExaminationDTO:
      type: object
      properties:
        normalBreathing:
          type: boolean
          example: true
          default: false
        dyspnoea:
          type: boolean
          example: false
          default: false
        constipation:
          type: boolean
          example: false
          default: false
        abnormalSounds:
          type: boolean
          example: false
          default: false
        asymmetricalMovements:
          type: boolean
          example: false
          default: false
        abnormalBreathing:
          type: boolean
          example: false
          default: false
        hyperventilation:
          type: boolean
          example: false
          default: false
        breathingFrequency1:
          type: integer
          example: 12
        breathingFrequency2:
          type: integer
          example: 10
        oxygenSaturation1:
          type: integer
          example: 97
        oxygenSaturation2:
          type: integer
          example: 95
    CirculatoryExaminationDTO:
      type: object
      properties:
        strongBleeding:
          type: boolean
          example: false
          default: false
        arrhythmia:
          type: boolean
          example: false
          default: false
        shock:
          type: boolean
          example: false
          default: false
        cardiacArrest:
          type: boolean
          example: false
          default: false
        burning:
          type: boolean
          example: false
          default: false
        burningFirstDegree:
          type: integer
          example: 18
        burningSecondDegree:
          type: integer
          example: 9
        burningThirdDegree:
          type: integer
          example: 9
        heartRate1:
          type: integer
          example: 68
        heartRate2:
          type: integer
          example: 83
        bloodPressure1:
          type: string
          example: 120/80
        bloodPressure2:
          type: string
          example: 145/100
    NeurologicalExaminationDTO:
      type: object
      properties:
        cramp:
          type: boolean
          example: false
          default: false
        speakingDisorder:
          type: boolean
          example: false
          default: false
        painEstimate:
          type: string
          example: mittel
        pupilReaction:
          $ref: '#/components/schemas/PupilReactionDTO'
        bloodSugar1:
          type: integer
          example: 120
        bloodSugar2:
          type: integer
          example: 97
        temp1:
          type: number
          example: 37.3
        temp2:
          type: number
          example: 36.8
        measurementTime1:
          type: string
          example: 13:14
        measurementTime2:
          type: string
          example: 13:51
      required:
        - pupilReaction
    PupilReactionDTO:
      type: object
      properties:
        pupilSmallLeft:
          type: boolean
          example: false
          default: false
        pupilSmallRight:
          type: boolean
          example: false
          default: false
        pupilMediumLeft:
          type: boolean
          example: true
          default: false
        pupilMediumRight:
          type: boolean
          example: true
          default: false
        pupilLargeLeft:
          type: boolean
          example: false
          default: false
        pupilLargeRight:
          type: boolean
          example: false
          default: false
        pupilLightUnresponsiveLeft:
          type: boolean
          example: false
          default: false
        pupilLightUnresponsiveRight:
          type: boolean
          example: false
          default: false
    FurtherExaminationDTO:
      type: object
      properties:
        allergies:
          type: array
          items:
            type: string
            example: Penicillin
          default: []
        meds:
          type: array
          items:
            type: string
            example: ThromboASS 20mg 1-0-1
          default: []
        patientHistory:
          type: string
          example: 2017 MI, mit 2 Stent versorgt KH WN
        beforeTheEvent:
          type: string
          example: aufgewacht, beim Frühstücken
      required:
        - allergies
        - meds
    PictureDTO:
      type: object
      properties:
        id:
          type: integer
          format: int64
          example: 17
        encoded:
          type: string
          description: Base64 encoded picture data
        name:
          type: string
          example: testpic.jpg
    ErrorDTO:
      type: object
      properties:
        type:
          $ref: '#/components/schemas/ErrorType'
        message:
          type: string
          example: 'Was für ein Fehler!'
    ErrorType:
      type: string
      enum: [ FATAL, WARN, INFO ]
    AddressDTO:
      type: object
      properties:
        street:
          type: string
          example: Testgasse
        housenr:
          type: string
          example: 3A/3
        postalcode:
          type: string
          example: 1020
        city:
          type: string
          example: Wien
        country:
          type: string
          example: Österreich
