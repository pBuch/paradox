import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { OperationsService } from '../services/operations.service';

@Injectable({
  providedIn: 'root'
})
export class HasOperationGuard implements CanActivate {

  constructor(private operationsService: OperationsService,
    private router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.operationsService.getActOperation() === undefined) {
      this.router.navigateByUrl('/');
    }
    return true;
  }

}
