import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Globals {
  static readonly defaultSnackbarDuration: number = 5 * 1000; // 5 seconds
  static readonly backendUri: string = 'http://localhost:8080';
  static readonly frontendUri: string = 'http://localhost:4200';
}
