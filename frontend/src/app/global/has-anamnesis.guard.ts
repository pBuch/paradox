import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AnamnesisService } from '../services/anamnesis.service';

@Injectable({
  providedIn: 'root'
})
export class HasAnamnesisGuard implements CanActivate {

  constructor(private anamnesisService: AnamnesisService,
    private router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.anamnesisService.getActAnamnesis() === undefined) {
      this.router.navigateByUrl('/phone-front-page');
    }
    return true;
  }
}
