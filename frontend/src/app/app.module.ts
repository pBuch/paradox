import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatDialogModule } from '@angular/material/dialog';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { MatDividerModule } from '@angular/material/divider';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatChipsModule } from '@angular/material/chips';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule } from '@angular/material/snack-bar';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { DatePipe } from '@angular/common';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatTabsModule } from '@angular/material/tabs';
import { MatListModule } from '@angular/material/list';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MAT_RADIO_DEFAULT_OPTIONS, MatRadioModule } from '@angular/material/radio';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { HeaderComponent } from './components/header/header.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { KonvaModule } from 'ng2-konva';


import { ParadoxCardComponent } from './components/cards/paradox-card/paradox-card.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PersonalDataCardComponent } from './components/cards/personal-data-card/personal-data-card.component';
import { BasicInfoPageComponent } from './components/pages/basic-info-page/basic-info-page.component';
import { TransportDataCardComponent } from './components/cards/transport-data-card/transport-data-card.component';
import { OperationsOverviewComponent } from './components/operations-overview/operations-overview.component';
import { OperationListComponent } from './components/operation-list/operation-list.component';
import { PatientPositionAssessmentCardComponent } from './components/cards/patient-position-assessment-card/patient-position-assessment-card.component';
import { ExaminationPageComponent } from './components/pages/examination-page/examination-page.component';
import { RespirationAssessmentCardComponent } from './components/cards/respiration-assessment-card/respiration-assessment-card.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { CirculatoryAssessmentCardComponent } from './components/cards/circulatory-assessment-card/circulatory-assessment-card.component';
import { PatientCriticalCardComponent } from './components/cards/patient-critical-card/patient-critical-card.component';
import { NeurologicalAssessmentCardComponent } from './components/cards/neurological-assessment-card/neurological-assessment-card.component';
import { FurtherAssessmentCardComponent } from './components/cards/further-assessment-card/further-assessment-card.component';
import { TextareaAutosizeModule } from 'ngx-textarea-autosize';
import { ForeignAnamnesisPageComponent } from './components/pages/foreign-anamnesis-page/foreign-anamnesis-page.component';
import { ForeignAnamnesisCardComponent } from './components/cards/foreign-anamnesis-card/foreign-anamnesis-card.component';
import { QuickScrollSidebarComponent } from './components/quick-scroll-sidebar/quick-scroll-sidebar.component';
import { PhoneFrontPageComponent } from './components/phone-components/phone-front-page/phone-front-page.component';
import { PhoneHeaderComponent } from './components/phone-components/phone-header/phone-header.component';
import { PhonePersonalDataComponent } from './components/phone-components/phone-personal-data/phone-personal-data.component';
import { PhoneOtherContactDataComponent } from './components/phone-components/phone-other-contact-data/phone-other-contact-data.component';
import { PhoneFurtherMedicalInfoDataComponent } from './components/phone-components/phone-further-medical-info-data/phone-further-medical-info-data.component';
import { PhoneMedicalPicturesComponent } from './components/phone-components/phone-medical-pictures/phone-medical-pictures.component';
import { PhoneOtherPicturesComponent } from './components/phone-components/phone-other-pictures/phone-other-pictures.component';
import { Globals } from './global/globals';
import { ErrorDialogComponent } from './components/dialogs/error-dialog/error-dialog.component';
import { PhoneContactOverviewComponent } from './components/phone-components/phone-contact-overview/phone-contact-overview.component';
import { QRCodeModule } from 'angularx-qrcode';
import { PainLocalisationPageComponent } from './components/pages/pain-localisation-page/pain-localisation-page.component';
import { AngularResizedEventModule } from 'angular-resize-event';
import { PainIndicatorDialogComponent } from './components/dialogs/pain-indicator-dialog/pain-indicator-dialog.component';
import { YesNoDialogComponent } from './components/dialogs/yes-no-dialog/yes-no-dialog.component';
import { ForeignAnamnesisSummaryCardComponent } from './components/cards/foreign-anamnesis-summary-card/foreign-anamnesis-summary-card.component';

@NgModule({
  declarations: [
    AppComponent,
    OperationsOverviewComponent,
    OperationListComponent,
    HeaderComponent,
    ParadoxCardComponent,
    PersonalDataCardComponent,
    BasicInfoPageComponent,
    TransportDataCardComponent,
    PatientPositionAssessmentCardComponent,
    ExaminationPageComponent,
    RespirationAssessmentCardComponent,
    CirculatoryAssessmentCardComponent,
    PatientCriticalCardComponent,
    NeurologicalAssessmentCardComponent,
    FurtherAssessmentCardComponent,
    ForeignAnamnesisPageComponent,
    ForeignAnamnesisCardComponent,
    QuickScrollSidebarComponent,
    PhoneFrontPageComponent,
    PhoneHeaderComponent,
    PhonePersonalDataComponent,
    PhoneOtherContactDataComponent,
    PhoneFurtherMedicalInfoDataComponent,
    PhoneMedicalPicturesComponent,
    PhoneOtherPicturesComponent,
    ErrorDialogComponent,
    PhoneContactOverviewComponent,
    PainLocalisationPageComponent,
    PainIndicatorDialogComponent,
    YesNoDialogComponent,
    ForeignAnamnesisSummaryCardComponent,
  ],
  imports: [
    FlexLayoutModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatPaginatorModule,
    MatDividerModule,
    MatGridListModule,
    MatRippleModule,
    MatProgressSpinnerModule,
    MatChipsModule,
    MatToolbarModule,
    MatMenuModule,
    MatSnackBarModule,
    MatButtonToggleModule,
    MatDialogModule,
    MatTableModule,
    MatDialogModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatNativeDateModule,
    NgxMaterialTimepickerModule,
    MatExpansionModule,
    MatBottomSheetModule,
    MatListModule,
    MatTooltipModule,
    MatTabsModule,
    MatRadioModule,
    MatSlideToggleModule,
    MatSidenavModule,
    MatCheckboxModule,
    TextareaAutosizeModule,
    ScrollToModule.forRoot(),
    QRCodeModule,
    KonvaModule,
    AngularResizedEventModule
  ],
  providers: [
    {
      provide: MAT_RADIO_DEFAULT_OPTIONS,
      useValue: { color: 'primary' }
    },
    {
      provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
      useValue: { duration: Globals.defaultSnackbarDuration }
    },
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
