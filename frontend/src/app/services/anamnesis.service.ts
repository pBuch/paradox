import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { AnamnesisApiService, AnamnesisState, ForeignAnamnesisInfoDTO, OperationState } from '../../generated';

@Injectable({
  providedIn: 'root'
})
export class AnamnesisService {

  private actAnamnesis: ForeignAnamnesisInfoDTO | null;

  constructor(private anamnesisApiService: AnamnesisApiService) {
  }

  initAnamnesisWithId(id: number): Observable<ForeignAnamnesisInfoDTO> {
    return this.anamnesisApiService.getForeignAnamnesis(id)
               .pipe(tap(anamnesis => this.actAnamnesis = anamnesis));
  }

  initNewAnamnesis(id: number): void {
    this.actAnamnesis = {
      start: new Date().toISOString(),
      state: OperationState.INPROGRESS,
      contactInfoAuthor: {},
      contacts: [],
      medicalInfoPatient: {},
      medicalAidOperationInfoID: id,
      medsPicture: {},
      decreePicture: {},
      otherPicture: {}
    };
  }

  getActAnamnesis(): ForeignAnamnesisInfoDTO | null {
    return this.actAnamnesis;
  }

  resetActAnamnesis(): void {
    this.actAnamnesis = null;
  }

  saveActAnamnesis(): Observable<ForeignAnamnesisInfoDTO> {
    if (this.actAnamnesis) {
      return this.anamnesisApiService.saveForeignAnamnesis(this.actAnamnesis)
                 .pipe(tap(newAnamnesis => {
                   this.actAnamnesis = newAnamnesis;
                 }));
    }
    return of();
  }

  completeAnamnesis(): Observable<void | ForeignAnamnesisInfoDTO> {
    if (this.actAnamnesis) {
      this.actAnamnesis.state = AnamnesisState.COMPLETED;
      this.actAnamnesis.end = new Date().toISOString();
      return this.anamnesisApiService.saveForeignAnamnesis(this.actAnamnesis)
                 .pipe(
                   catchError(error => {
                     if (this.actAnamnesis) {
                       this.actAnamnesis.state = AnamnesisState.INPROGRESS;
                       this.actAnamnesis.end = undefined;
                     }
                     return throwError(error.error.message.split('\n'));
                   })
                 );
    }
    return of();
  }
}
