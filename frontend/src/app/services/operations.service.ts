import { Injectable } from '@angular/core';
import { MedicalAidOperationInfoDTO, OperationsApiService, OperationState } from '../../generated';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OperationsService {

  private actOperation: MedicalAidOperationInfoDTO;

  constructor(private operationsApiService: OperationsApiService) {
  }

  getTodaysOperations(finished: boolean): Observable<Array<MedicalAidOperationInfoDTO>> {
    return this.operationsApiService.getTodaysOperations(finished);
  }

  initOperationWithId(id: number): Observable<MedicalAidOperationInfoDTO> {
    return this.operationsApiService.getOperationById(id)
      .pipe(tap(operation => this.actOperation = operation));
  }

  initNewOperation(): void {
    this.actOperation = {
      start: new Date().toISOString(),
      state: OperationState.INPROGRESS,
      basicInfo: {
        personalInfo: {
          address: {}
        },
        transportInfo: {}
      },
      examination: {
        patientPosition: {},
        respiratoryExamination: {},
        circulatoryExamination: {},
        neurologicalExamination: {
          pupilReaction: {}
        },
        furtherExamination: {
          allergies: [''],
          meds: ['']
        }
      },
      painLocalisation: {
        indicators: []
      },
      foreignAnamnesisID: -1,
    };
  }

  chooseOperation(operation: MedicalAidOperationInfoDTO): void {
    this.initNewOperation();
    this.actOperation = Object.assign(this.actOperation, operation);
  }

  getActOperation(): MedicalAidOperationInfoDTO {
    return this.actOperation;
  }

  saveActOperation(): Observable<void> {
    // console.log(this.actOperation);
    return this.operationsApiService.saveOperation(this.actOperation)
               .pipe(map(newOperation => {
                 this.actOperation = newOperation;
               }));
  }

  completeOperation(): Observable<void | MedicalAidOperationInfoDTO> {
    this.actOperation.state = OperationState.COMPLETED;
    this.actOperation.end = new Date().toISOString();
    return this.operationsApiService.saveOperation(this.actOperation)
               .pipe(
                 catchError(error => {
                   this.actOperation.state = OperationState.INPROGRESS;
                   this.actOperation.end = undefined;
                   return throwError(error.error.message.split('\n'));
                 })
               );
  }
}
