import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PictureDTO } from '../../generated';

@Injectable({
  providedIn: 'root'
})
export class PictureService {

  private static getValidFileType(imageName: string | undefined): string | null {
    if (imageName === undefined) {
      return null;
    }
    const lowerPicName = imageName.toLowerCase();
    if (lowerPicName.endsWith('jpg') || lowerPicName.endsWith('jpeg')) {
      return 'jpg';
    }
    if (lowerPicName.endsWith('png')) {
      return 'png';
    }
    return null;
  }

  readImageIntoEncodedString(image: File): Observable<string | null> {
    const reader = new FileReader();
    const observable = new Observable<string | null>(observer => {
      reader.onload = readerEvent => {
        if (readerEvent.target === null) {
          observer.next(null);
        } else {
          const binaryString = readerEvent.target.result;
          observer.next(btoa(binaryString as string));
        }
      };
    });
    reader.readAsBinaryString(image);
    return observable;
  }

  pictureEncodedStringToDataURL(picture: PictureDTO): string | null {
    const fileType = PictureService.getValidFileType(picture.name);
    if (fileType === null) {
      return null;
    }
    return 'data:image/' + fileType + ';base64,' + picture.encoded;
  }
}
