import { ElementRef } from '@angular/core';

export class QuickScrollDestination {

  get offsetTop(): number {
    return this.destination.nativeElement.offsetTop;
  }

  get offsetHeight(): number {
    return this.destination.nativeElement.offsetHeight;
  }

  public scrollPosition: number;

  constructor(
    public iconPath: string,
    public destination: ElementRef,
    public active: boolean,
    public important: boolean = false
  ) {

  }
}
