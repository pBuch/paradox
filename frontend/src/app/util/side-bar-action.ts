export class SideBarAction {

  constructor(
    public matIcon: String,
    public callback: () => void
  ) {
  }
}
