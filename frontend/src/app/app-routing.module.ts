import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OperationsOverviewComponent } from './components/operations-overview/operations-overview.component';
import { BasicInfoPageComponent } from './components/pages/basic-info-page/basic-info-page.component';
import { ExaminationPageComponent } from './components/pages/examination-page/examination-page.component';
import { ForeignAnamnesisPageComponent } from './components/pages/foreign-anamnesis-page/foreign-anamnesis-page.component';
import { PhoneFrontPageComponent } from './components/phone-components/phone-front-page/phone-front-page.component';
import { PhonePersonalDataComponent } from './components/phone-components/phone-personal-data/phone-personal-data.component';
import { PhoneOtherContactDataComponent } from './components/phone-components/phone-other-contact-data/phone-other-contact-data.component';
import { PhoneFurtherMedicalInfoDataComponent } from './components/phone-components/phone-further-medical-info-data/phone-further-medical-info-data.component';
import { PhoneMedicalPicturesComponent } from './components/phone-components/phone-medical-pictures/phone-medical-pictures.component';
import { PhoneOtherPicturesComponent } from './components/phone-components/phone-other-pictures/phone-other-pictures.component';
import { HasOperationGuard } from './global/has-operation.guard';
import { PhoneContactOverviewComponent } from './components/phone-components/phone-contact-overview/phone-contact-overview.component';
import { HasAnamnesisGuard } from './global/has-anamnesis.guard';
import { PainLocalisationPageComponent } from './components/pages/pain-localisation-page/pain-localisation-page.component';

const routes: Routes = [
  { path: '', component: OperationsOverviewComponent },
  { path: 'basic-info', component: BasicInfoPageComponent, canActivate: [HasOperationGuard] },
  { path: 'examination', component: ExaminationPageComponent, canActivate: [HasOperationGuard] },
  { path: 'pain-localisation', component: PainLocalisationPageComponent, canActivate: [HasOperationGuard] },
  { path: 'foreign-anamnesis', component: ForeignAnamnesisPageComponent, canActivate: [HasOperationGuard] },
  { path: 'phone-front-page/:operationId/:anamnesisId', component: PhoneFrontPageComponent },
  { path: 'phone-personal-data', component: PhonePersonalDataComponent },
  { path: 'phone-other-data/:id', component: PhoneOtherContactDataComponent },
  { path: 'phone-further-medical-data', component: PhoneFurtherMedicalInfoDataComponent },
  { path: 'phone-medical-pictures', component: PhoneMedicalPicturesComponent },
  { path: 'phone-other-pictures', component: PhoneOtherPicturesComponent },
  { path: 'phone-contact-overview', component: PhoneContactOverviewComponent, canActivate: [HasAnamnesisGuard] }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
