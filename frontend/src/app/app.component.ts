import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'pdx-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ParaDox';

  constructor(public router: Router) { }
}
