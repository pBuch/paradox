import { Component, OnInit } from '@angular/core';
import { MedicalAidOperationInfoDTO } from '../../../generated';
import { Router } from '@angular/router';
import { OperationsService } from '../../services/operations.service';
import { AnamnesisService } from '../../services/anamnesis.service';

@Component({
  selector: 'pdx-operations-overview',
  templateUrl: './operations-overview.component.html',
  styleUrls: ['./operations-overview.component.scss']
})
export class OperationsOverviewComponent implements OnInit {

  doneOperations: MedicalAidOperationInfoDTO[];
  ongoingOperations: MedicalAidOperationInfoDTO[];

  constructor(private router: Router,
    private operationsService: OperationsService,
    private anamnesisService: AnamnesisService) {
  }

  ngOnInit(): void {
    this.operationsService.getTodaysOperations(false)
        .subscribe(operations => this.ongoingOperations = operations);

    this.operationsService.getTodaysOperations(true)
        .subscribe(operations => this.doneOperations = operations);
    this.anamnesisService.resetActAnamnesis();
  }

  createNewOperation() {
    this.operationsService.initNewOperation();
    this.router.navigateByUrl('/basic-info');
  }

}
