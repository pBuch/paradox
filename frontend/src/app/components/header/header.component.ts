import { Component, Input, OnInit } from '@angular/core';
import { OperationsService } from '../../services/operations.service';

@Component({
  selector: 'pdx-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  public links = [
      { name: 'Allgemeines', link: '/basic-info' },
      { name: 'Behandlung', link: '/examination'},
      { name: 'Schmerz-/Wundlokalisation', link: '/pain-localisation'},
      { name: 'Fremdanamnese', link: '/foreign-anamnesis'}
    ];

  public activeLink: any;

  constructor(private operationsService: OperationsService) { }

  ngOnInit(): void {
    this.activeLink = this.links[0];
  }

  get patientName(): string {
    if (this.operationsService.getActOperation().basicInfo.personalInfo.name) {
      return this.operationsService.getActOperation().basicInfo.personalInfo.name as string;
    }
    return '???';
  }

  get startTime(): Date {
    return new Date(this.operationsService.getActOperation().start);
  }

  get endTime(): Date | null {
    if (this.operationsService.getActOperation().end) {
      return new Date(this.operationsService.getActOperation().end as string);
    }
    return null;
  }

}
