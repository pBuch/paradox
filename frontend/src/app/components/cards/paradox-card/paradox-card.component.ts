import { Component, Input, OnInit, TemplateRef } from '@angular/core';

@Component({
  selector: 'pdx-paradox-card',
  templateUrl: './paradox-card.component.html',
  styleUrls: ['./paradox-card.component.scss']
})
export class ParadoxCardComponent implements OnInit {

  @Input() contentTemplate: TemplateRef<any>;
  @Input() headerTemplate: TemplateRef<any>;
  @Input() srcPath: string;
  @Input() title: string;
  @Input() criticalStyle = false;
  @Input() showStartMargin = false;
  @Input() showEndMargin = true;
  @Input() contentPadding = true;
  constructor() { }

  ngOnInit(): void {
  }

}
