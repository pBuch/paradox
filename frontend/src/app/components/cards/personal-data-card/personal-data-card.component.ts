import { Component } from '@angular/core';
import { OperationsService } from '../../../services/operations.service';
import { MedicalAidOperationInfoDTO } from '../../../../generated';

@Component({
  selector: 'pdx-personal-data-card',
  templateUrl: './personal-data-card.component.html',
  styleUrls: ['./personal-data-card.component.scss']
})
export class PersonalDataCardComponent {

  constructor(private operationsService: OperationsService) { }

  get actOperation(): MedicalAidOperationInfoDTO {
    return this.operationsService.getActOperation();
  }

}
