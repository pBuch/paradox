import { Component } from '@angular/core';
import { OperationsService } from '../../../services/operations.service';
import { MedicalAidOperationInfoDTO } from '../../../../generated';

@Component({
  selector: 'pdx-patient-critical-card',
  templateUrl: './patient-critical-card.component.html',
  styleUrls: ['./patient-critical-card.component.scss']
})
export class PatientCriticalCardComponent {

  constructor(private operationsService: OperationsService) {
  }

  get actOperation(): MedicalAidOperationInfoDTO {
    return this.operationsService.getActOperation();
  }
}
