import { Component } from '@angular/core';
import { OperationsService } from '../../../services/operations.service';
import { PatientPositionDTO } from '../../../../generated';

@Component({
  selector: 'pdx-patient-position-assessment-card',
  templateUrl: './patient-position-assessment-card.component.html',
  styleUrls: ['./patient-position-assessment-card.component.scss']
})
export class PatientPositionAssessmentCardComponent {

  constructor(private operationService: OperationsService) {
  }

  get patientPosition(): PatientPositionDTO {
    return this.operationService.getActOperation().examination.patientPosition;
  }

}
