import { Component, OnInit } from '@angular/core';
import { ForeignAnamnesisInfoDTO, PictureDTO } from '../../../../generated';
import { AnamnesisService } from '../../../services/anamnesis.service';
import { PictureService } from '../../../services/picture.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'pdx-foreign-anamnesis-summary-card',
  templateUrl: './foreign-anamnesis-summary-card.component.html',
  styleUrls: ['./foreign-anamnesis-summary-card.component.scss']
})
export class ForeignAnamnesisSummaryCardComponent {

  constructor(private anamnesisService: AnamnesisService,
    private pictureService: PictureService,
    private snackbar: MatSnackBar) { }

  get anamnesis(): ForeignAnamnesisInfoDTO | null {
    return this.anamnesisService.getActAnamnesis();
  }

  hasPictureInfo(picture: PictureDTO): boolean {
    return !!picture.name;
  }

  toPictureUrl(picture: PictureDTO): string {
    const url = this.pictureService.pictureEncodedStringToDataURL(picture);
    if (!url) {
      this.snackbar.open('Could not open picture because of an encoding issue');
      return '';
    }
    return url;
  }

}
