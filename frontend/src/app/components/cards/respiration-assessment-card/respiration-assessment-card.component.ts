import { Component } from '@angular/core';
import { OperationsService } from '../../../services/operations.service';
import { RespiratoryExaminationDTO } from '../../../../generated';

@Component({
  selector: 'pdx-respiration-assessment-card',
  templateUrl: './respiration-assessment-card.component.html',
  styleUrls: ['./respiration-assessment-card.component.scss']
})
export class RespirationAssessmentCardComponent {

  constructor(private operationsService: OperationsService) { }

  get respiratoryExaminationDTO(): RespiratoryExaminationDTO {
    return this.operationsService.getActOperation().examination.respiratoryExamination;
  }
}
