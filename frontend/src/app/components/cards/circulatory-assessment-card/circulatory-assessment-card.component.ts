import { Component } from '@angular/core';
import { OperationsService } from '../../../services/operations.service';
import { CirculatoryExaminationDTO } from '../../../../generated';

@Component({
  selector: 'pdx-circulatory-assessment-card',
  templateUrl: './circulatory-assessment-card.component.html',
  styleUrls: ['./circulatory-assessment-card.component.scss']
})
export class CirculatoryAssessmentCardComponent {

  constructor(private operationsService: OperationsService) {
  }

  get circulatoryExamination(): CirculatoryExaminationDTO {
    return this.operationsService.getActOperation().examination.circulatoryExamination;
  }

}
