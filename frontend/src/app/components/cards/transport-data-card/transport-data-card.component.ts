import { Component, OnInit } from '@angular/core';
import { OperationsService } from '../../../services/operations.service';
import { MedicalAidOperationInfoDTO } from '../../../../generated';

@Component({
  selector: 'pdx-transport-data-card',
  templateUrl: './transport-data-card.component.html',
  styleUrls: ['./transport-data-card.component.scss']
})
export class TransportDataCardComponent {

  constructor(private operationsService: OperationsService) {
  }

  get actOperation(): MedicalAidOperationInfoDTO {
    return this.operationsService.getActOperation();
  }

}
