import { Component } from '@angular/core';
import { OperationsService } from '../../../services/operations.service';
import { FurtherExaminationDTO } from '../../../../generated';

@Component({
  selector: 'pdx-further-assessment-card',
  templateUrl: './further-assessment-card.component.html',
  styleUrls: ['./further-assessment-card.component.scss']
})
export class FurtherAssessmentCardComponent {

  constructor(private operationsService: OperationsService) {
  }

  get furtherExamination(): FurtherExaminationDTO {
    return this.operationsService.getActOperation().examination.furtherExamination;
  }

  onAddAllergy(): void {
    this.furtherExamination.allergies.push('');
  }

  onRemoveAllergy(): void {
    this.furtherExamination.allergies.pop();
  }

  onAddMedicament(): void {
    this.furtherExamination.meds.push('');
  }

  onRemoveMedicament(): void {
    this.furtherExamination.meds.pop();
  }

  trackByIndex(index: number, item: any): number {
    return index;
  }

}
