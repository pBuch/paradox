import { Component } from '@angular/core';
import { OperationsService } from '../../../services/operations.service';
import { NeurologicalExaminationDTO } from '../../../../generated';

@Component({
  selector: 'pdx-neurological-assessment-card',
  templateUrl: './neurological-assessment-card.component.html',
  styleUrls: ['./neurological-assessment-card.component.scss']
})
export class NeurologicalAssessmentCardComponent {

  constructor(private operationsService: OperationsService) { }

  get neurologicalExamination(): NeurologicalExaminationDTO {
    return this.operationsService.getActOperation().examination.neurologicalExamination;
  }

}
