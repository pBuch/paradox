import { Component } from '@angular/core';
import { OperationsService } from '../../../services/operations.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Globals } from '../../../global/globals';
import { PictureService } from '../../../services/picture.service';
import { AnamnesisService } from '../../../services/anamnesis.service';
import { ForeignAnamnesisInfoDTO } from '../../../../generated';

@Component({
  selector: 'pdx-foreign-anamnesis-card',
  templateUrl: './foreign-anamnesis-card.component.html',
  styleUrls: ['./foreign-anamnesis-card.component.scss']
})
export class ForeignAnamnesisCardComponent {

  qrCodeContent: string;

  constructor(private operationsService: OperationsService,
    private pictureService: PictureService,
    private snackBar: MatSnackBar,
    private router: Router,
    private anamnesisService: AnamnesisService) {
  }

  onCreateForeignAnamnesis(): void {
    if (!this.hasSvnr()) {
      this.snackBar.open('Keine Sozialversicherungsnummer hinterlegt!');
    } else {
      if (this.operationsService.getActOperation().id === undefined) {
        this.operationsService.saveActOperation().subscribe(() => {
          this.createForeignAnamnesis().then(() => this.onOpenForeignAnamnesis());
        });
      } else {
        this.createForeignAnamnesis().then(() => this.onOpenForeignAnamnesis());
      }
    }
  }

  createQrCode(): void {
    if (this.hasForeignAnamnesis()) {
      this.showQrCode();
      return;
    }
    if (!this.hasSvnr()) {
      this.snackBar.open('Keine Sozialversicherungsnummer hinterlegt!');
    } else {
      if (this.operationsService.getActOperation().id === undefined) {
        this.operationsService.saveActOperation().subscribe(() => {
          this.createForeignAnamnesis().then(() => this.showQrCode());
        });
      } else {
        this.createForeignAnamnesis().then(() => this.showQrCode());
      }
    }
  }

  private showQrCode() {
    this.qrCodeContent = Globals.frontendUri + this.getForeignAnamnesisUrl();
  }

  private hasSvnr(): boolean {
    return !!this.operationsService.getActOperation().basicInfo.personalInfo.svnr;
  }

  hasForeignAnamnesis(): boolean {
    const id = this.operationsService.getActOperation().foreignAnamnesisID;
    if (id === undefined) {
      return false;
    }
    if (id > 0) {
      return true;
    }
    return false;
  }

  private createForeignAnamnesis(): Promise<any> {
    return new Promise(((resolve, reject) => {
      this.anamnesisService.initNewAnamnesis(this.operationsService.getActOperation().id as number);
      this.anamnesisService.saveActAnamnesis().subscribe(foreignInfo => {
        this.operationsService.getActOperation().foreignAnamnesisID = (foreignInfo as ForeignAnamnesisInfoDTO).id;
        resolve();
      });
    }));

  }

  onOpenForeignAnamnesis(): void {
    this.router.navigateByUrl(this.getForeignAnamnesisUrl());
  }

  private getForeignAnamnesisUrl(): string {
    const operationId = this.operationsService.getActOperation().id;
    const anamnesisID = this.operationsService.getActOperation().foreignAnamnesisID;
    if (!anamnesisID || !operationId) {
      console.error('No ID!');
      return '';
    }
    return '/phone-front-page/' + operationId + '/' + anamnesisID;
  }
}
