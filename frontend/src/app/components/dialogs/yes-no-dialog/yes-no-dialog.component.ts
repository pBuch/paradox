import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PainIndicatorDTO } from '../../../../generated';

@Component({
  selector: 'pdx-yes-no-dialog',
  templateUrl: './yes-no-dialog.component.html',
  styleUrls: ['./yes-no-dialog.component.scss']
})
export class YesNoDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {title: string, content: string},
    public dialogRef: MatDialogRef<YesNoDialogComponent>) { }

  ngOnInit(): void {
  }

  public onYesClicked() {
    this.dialogRef.close(true);
  }

  public onNoClicked() {
    this.dialogRef.close(false);
  }

}
