import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'pdx-error-dialog',
  templateUrl: './error-dialog.component.html',
  styleUrls: ['./error-dialog.component.scss']
})
export class ErrorDialogComponent implements OnInit {

  errors: String[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { errors: String[] },
    public dialogRef: MatDialogRef<ErrorDialogComponent>) {
  }

  ngOnInit(): void {
  }

  onCloseClicked() {
    this.dialogRef.close();
  }
}
