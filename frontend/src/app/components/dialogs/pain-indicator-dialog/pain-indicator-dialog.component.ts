import { Component, Inject, NgZone, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PainIndicatorDTO } from '../../../../generated';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { take } from 'rxjs/operators';

@Component({
  selector: 'pdx-pain-indicator-dialog',
  templateUrl: './pain-indicator-dialog.component.html',
  styleUrls: ['./pain-indicator-dialog.component.scss']
})
export class PainIndicatorDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public indicator: PainIndicatorDTO,
    public dialogRef: MatDialogRef<PainIndicatorDialogComponent>) { }

  ngOnInit(): void {

  }

  public onOkClicked() {
    this.dialogRef.close();
  }

}
