import { Component, OnInit } from '@angular/core';
import { OperationsService } from '../../../services/operations.service';
import { PictureService } from '../../../services/picture.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AnamnesisService } from '../../../services/anamnesis.service';
import { ForeignAnamnesisInfoDTO } from '../../../../generated';

@Component({
  selector: 'pdx-phone-other-pictures',
  templateUrl: './phone-other-pictures.component.html',
  styleUrls: ['./phone-other-pictures.component.scss'],
  host: { class: 'apply_flex' }
})
export class PhoneOtherPicturesComponent implements OnInit {
  showOtherPic: boolean;

  constructor(private operationsService: OperationsService,
    private pictureService: PictureService,
    private snackBar: MatSnackBar,
    private router: Router,
    private anamnesisService: AnamnesisService) {
  }

  ngOnInit(): void {
    if (this.anamnesisService.getActAnamnesis() != null) {
      const pic = this.anamnesisService.getActAnamnesis()?.otherPicture.encoded;
      if (pic === '' || pic === null) {
        this.hidePicture(1);
      } else {
        this.showPicture(1);
      }
    }
  }

  get anamnesis(): ForeignAnamnesisInfoDTO | null {
    return this.anamnesisService.getActAnamnesis();
  }

  onImageSelected(event: Event, id: number) {
    const target = event.target as HTMLInputElement;
    if (target.files === null || target.files.length === 0) {

      this.snackBar.open('Bitte eine Bilddatei angeben!');
      return;
    }
    const imageFile = target.files[0];
    const picName = imageFile.name;

    this.pictureService.readImageIntoEncodedString(imageFile)
        .subscribe(encodedString => {
          if (encodedString === null) {
            this.snackBar.open('Fehler bei der Bildverarbeitung');
            this.clearPicture(id);
          } else {
            const anamnesis = this.anamnesisService.getActAnamnesis();
            if (id === 1 && anamnesis) {
              anamnesis.otherPicture = {
                encoded: encodedString,
                name: picName
              };
            }
            this.showPicture(id);
            if (this.pictureURL === null) {
              this.snackBar.open('Falsches Bildformat - bitte JPG oder PNG nutzen');
              this.clearPicture(id);
            }
          }
        }, error => this.snackBar.open('Fehler bei der Bildverarbeitung'));
  }

  pictureURL(id: number): string | null {
    const anamnesis = this.anamnesisService.getActAnamnesis();
    if (id === 1 && anamnesis) {
      return this.pictureService.pictureEncodedStringToDataURL(anamnesis.otherPicture);
    }
    return null;
  }

  private clearPicture(id: number): void {
    if (id === 1) {
      const anamnesis = this.anamnesisService.getActAnamnesis();
      if (anamnesis) {
        anamnesis.otherPicture.encoded = '';
        anamnesis.otherPicture.name = '';
      }
    }
    this.hidePicture(id);
  }

  onOpenPic(id: string) {
    const input = document.getElementById(id);
    if (input !== null) {
      input.click();
    }
  }

  private showPicture(id: number) {
    if (id === 1) {
      this.showOtherPic = true;
    }
  }

  private hidePicture(id: number) {
    if (id === 1) {
      this.showOtherPic = false;
    }
  }

  completeAnamnesis(): void {
    this.anamnesisService.completeAnamnesis().subscribe();
    this.router.navigateByUrl('/');
  }
}
