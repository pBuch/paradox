import { Component, OnInit } from '@angular/core';
import { OperationsService } from '../../../services/operations.service';

@Component({
  selector: 'pdx-phone-header',
  templateUrl: './phone-header.component.html',
  styleUrls: ['./phone-header.component.scss']
})
export class PhoneHeaderComponent implements OnInit {

  constructor(private operationService: OperationsService) { }

  ngOnInit(): void {
  }

}
