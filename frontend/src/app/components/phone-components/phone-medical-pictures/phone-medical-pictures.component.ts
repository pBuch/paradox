import { Component, OnInit } from '@angular/core';
import { OperationsService } from '../../../services/operations.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PictureService } from '../../../services/picture.service';
import { AnamnesisService } from '../../../services/anamnesis.service';

@Component({
  selector: 'pdx-phone-medical-pictures',
  templateUrl: './phone-medical-pictures.component.html',
  styleUrls: ['./phone-medical-pictures.component.scss'],
  host: { class: 'apply_flex' }
})
export class PhoneMedicalPicturesComponent implements OnInit {
  showMedPic: boolean;
  showDecreePic: boolean;


  constructor(private operationsService: OperationsService,
    private snackBar: MatSnackBar,
    private pictureService: PictureService,
    private anamnesisService: AnamnesisService) {
  }

  ngOnInit(): void {
    const anamnesis = this.anamnesisService.getActAnamnesis();
    if (!!anamnesis) {
      let pic = anamnesis.medsPicture.encoded;
      if (pic === '' || pic === null) {
        this.hidePicture(1);
      } else {
        this.showPicture(1);
      }
      pic = anamnesis.decreePicture.encoded;
      if (pic === '' || pic === null) {
        this.hidePicture(2);
      } else {
        this.showPicture(2);
      }
    }

  }

  onImageSelected(event: Event, id: number) {
    const target = event.target as HTMLInputElement;
    if (target.files === null || target.files.length === 0) {
      this.snackBar.open('Bitte eine Bilddatei angeben!');
      return;
    }
    const imageFile = target.files[0];
    const picName = imageFile.name;

    this.pictureService.readImageIntoEncodedString(imageFile)
        .subscribe(encodedString => {
          const anamnesis = this.anamnesisService.getActAnamnesis();
          if (encodedString === null || !anamnesis) {
            this.snackBar.open('Fehler bei der Bildverarbeitung');
            this.clearPicture(id);
          } else {
            if (id === 1) {
              anamnesis.medsPicture = {
                encoded: encodedString,
                name: picName
              };
            }
            if (id === 2) {
              anamnesis.decreePicture = {
                encoded: encodedString,
                name: picName
              };
            }
            this.showPicture(id);
          }
        }, error => this.snackBar.open('Fehler bei der Bildverarbeitung'));
  }

  pictureURL(id: number): string | null {
    const anamnesis = this.anamnesisService.getActAnamnesis();
    if (!!anamnesis) {
      if (id === 1) {
        return this.pictureService.pictureEncodedStringToDataURL(anamnesis.medsPicture);
      }
      if (id === 2) {
        return this.pictureService.pictureEncodedStringToDataURL(anamnesis.decreePicture);
      } else {
        return null;
      }
    }
    return null;
  }

  private showPicture(id: number) {
    if (id === 1) {
      this.showMedPic = true;
    } else if (id === 2) {
      this.showDecreePic = true;
    }
  }

  private hidePicture(id: number) {
    if (id === 1) {
      this.showMedPic = false;
    } else if (id === 2) {
      this.showDecreePic = false;
    }
  }

  private clearPicture(id: number): void {
    const anamnesis = this.anamnesisService.getActAnamnesis();
    if (anamnesis) {
      if (id === 1) {
        anamnesis.medsPicture.encoded = '';
        anamnesis.medsPicture.name = '';
      } else if (id === 2) {
        anamnesis.decreePicture.encoded = '';
        anamnesis.decreePicture.name = '';
      }
      this.hidePicture(id);
    }
  }

  onOpenPic(id: string) {
    const input = document.getElementById(id);
    if (input !== null) {
      input.click();
    }
  }

  save() {
    this.anamnesisService.saveActAnamnesis().subscribe();
  }
}
