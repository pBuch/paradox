import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AnamnesisService } from '../../../services/anamnesis.service';
import { OperationsService } from '../../../services/operations.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'pdx-phone-front-page',
  templateUrl: './phone-front-page.component.html',
  styleUrls: ['./phone-front-page.component.scss']
})
export class PhoneFrontPageComponent implements OnInit {

  constructor(private router: Router, private anamnesisService: AnamnesisService,
    public operationsService: OperationsService, private snackbar: MatSnackBar,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.operationsService.initOperationWithId(params['operationId']).subscribe();
      const contact = this.anamnesisService.initAnamnesisWithId(params['anamnesisId']).subscribe();
      if (!contact) {
        this.snackbar.open('No contact with this id!');
      }
    });
  }

  checkSvnr(svnrCheck: string) {
    if (this.operationsService.getActOperation().basicInfo.personalInfo.svnr === svnrCheck) {
      this.router.navigateByUrl('/phone-personal-data');
  } else {
      this.snackbar.open('Sozialversicherungsnummer ist falsch:');
    }
  }

  get isReady(): boolean {
    return !!this.operationsService.getActOperation() &&
      !!this.anamnesisService.getActAnamnesis();
  }
}
