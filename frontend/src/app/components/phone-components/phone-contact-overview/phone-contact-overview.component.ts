import { Component, OnInit } from '@angular/core';
import { AnamnesisService } from '../../../services/anamnesis.service';
import { OtherContactInfoDTO } from '../../../../generated';

@Component({
  selector: 'pdx-phone-contact-overview',
  templateUrl: './phone-contact-overview.component.html',
  styleUrls: ['./phone-contact-overview.component.scss'],
  host: { class: 'apply_flex' }
})
export class PhoneContactOverviewComponent {

  constructor(private anamnesisService: AnamnesisService) {
  }

  get contacts(): OtherContactInfoDTO[] {
    const anamnesis = this.anamnesisService.getActAnamnesis();
    if (anamnesis) {
      return anamnesis.contacts;
    }
    return [];
  }

  onAddContact(): void {
    const anamnesis = this.anamnesisService.getActAnamnesis();
    if (anamnesis) {
      anamnesis.contacts.push({
        name: '',
        relation: '',
        telnr: '',
        stationary: undefined,
        released: undefined
      });
    }
    this.anamnesisService.saveActAnamnesis().subscribe();
  }

  onDeleteContact(id: number): void {
    const anamnesis = this.anamnesisService.getActAnamnesis();
    if (anamnesis) {
      anamnesis.contacts = anamnesis.contacts.filter(contact => contact.id !== id);
    }

    this.anamnesisService.saveActAnamnesis().subscribe();
  }
}
