import { Component, OnInit } from '@angular/core';
import { AnamnesisService } from '../../../services/anamnesis.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { OtherContactInfoDTO } from '../../../../generated';

@Component({
  selector: 'pdx-phone-other-contact-data',
  templateUrl: './phone-other-contact-data.component.html',
  styleUrls: ['./phone-other-contact-data.component.scss'],
  host: { class: 'apply_flex' }
})
export class PhoneOtherContactDataComponent implements OnInit {

  actContact: OtherContactInfoDTO;

  constructor(private anamnesisService: AnamnesisService, private router: Router,
    private snackbar: MatSnackBar, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      const contact = this.anamnesisService.getActAnamnesis()?.contacts
                            .find(c => c.id?.toString() === params['id']);
      if (!contact) {
        this.snackbar.open('No contact with this id!');
      } else {
        this.actContact = contact;
      }
    });
  }

  saveData() {
    this.anamnesisService.saveActAnamnesis().subscribe(contact => {
      this.snackbar.open('Erfolgreich gespeichert!');
      this.router.navigateByUrl('/phone-contact-overview');
    });
  }
}
