import { Component, OnInit } from '@angular/core';
import { AnamnesisService } from '../../../services/anamnesis.service';
import { ForeignAnamnesisInfoDTO } from '../../../../generated';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'pdx-phone-personal-data',
  templateUrl: './phone-personal-data.component.html',
  styleUrls: ['./phone-personal-data.component.scss'],
  host: { class: 'apply_flex' }
})
export class PhonePersonalDataComponent {

  constructor(private anamnesisService: AnamnesisService, private router: Router,
    private snackbar: MatSnackBar) { }

  get actAnamnesis(): ForeignAnamnesisInfoDTO | null {
    return this.anamnesisService.getActAnamnesis();
  }

  saveData() {
    this.anamnesisService.saveActAnamnesis().subscribe( () => {
      this.snackbar.open('Erfolgreich gespeichert!');
    });
    this.router.navigateByUrl('/phone-contact-overview');
  }
}

