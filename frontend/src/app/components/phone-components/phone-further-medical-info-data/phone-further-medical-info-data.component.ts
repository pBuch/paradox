import { Component, OnInit } from '@angular/core';
import { AnamnesisService } from '../../../services/anamnesis.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ForeignAnamnesisInfoDTO } from '../../../../generated';

@Component({
  selector: 'pdx-phone-further-medical-info-data',
  templateUrl: './phone-further-medical-info-data.component.html',
  styleUrls: ['./phone-further-medical-info-data.component.scss'],
  host: {class: 'apply_flex'}
})
export class PhoneFurtherMedicalInfoDataComponent {

  constructor(private anamnesisService: AnamnesisService, private router: Router,
    private snackbar: MatSnackBar) { }

  get actAnamnesis(): ForeignAnamnesisInfoDTO | null {
    return this.anamnesisService.getActAnamnesis();
  }

  saveData() {
    this.anamnesisService.saveActAnamnesis().subscribe( () => {
      this.snackbar.open('Erfolgreich gespeichert!');
    });
    this.router.navigateByUrl('/phone-medical-pictures');
  }

}
