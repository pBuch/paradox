import { Component, OnInit } from '@angular/core';
import { AnamnesisService } from '../../../services/anamnesis.service';
import { OperationsService } from '../../../services/operations.service';
import { AnamnesisState } from '../../../../generated';

@Component({
  selector: 'pdx-foreign-anamnesis-page',
  templateUrl: './foreign-anamnesis-page.component.html',
  styleUrls: ['./foreign-anamnesis-page.component.scss'],
  host: { class: 'apply_flex' }
})

export class ForeignAnamnesisPageComponent implements OnInit {

  constructor(private anamnesisService: AnamnesisService,
    private operationService: OperationsService) {
  }

  ngOnInit(): void {
    const anamnesisId = this.operationService.getActOperation().foreignAnamnesisID;
    if (!!anamnesisId) {
      this.anamnesisService.initAnamnesisWithId(anamnesisId).subscribe();
    }
  }

  isForeignAnamnesisFinished(): boolean {
    const anamnesis = this.anamnesisService.getActAnamnesis();
    if (anamnesis) {
      return anamnesis.state === AnamnesisState.COMPLETED;
    }
    return false;
  }

}
