import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'pdx-basic-info-page',
  templateUrl: './basic-info-page.component.html',
  styleUrls: ['./basic-info-page.component.scss'],
  host: {class: 'apply_flex'},
})
export class BasicInfoPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
