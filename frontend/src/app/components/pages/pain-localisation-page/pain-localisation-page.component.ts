import { AfterViewInit, Component, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Observable, of, Subscription } from 'rxjs';
import { CoreShapeComponent, KonvaComponent } from 'ng2-konva';
import Konva from 'konva';
import { ResizedEvent } from 'angular-resize-event';
import { KonvaEventObject } from 'konva/lib/Node';
import { Shape } from 'konva/lib/Shape';
import { Key } from '../../../util/key.enum';
import { OperationsService } from '../../../services/operations.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { PainIndicatorDialogComponent } from '../../dialogs/pain-indicator-dialog/pain-indicator-dialog.component';
import { SideBarAction } from '../../../util/side-bar-action';
import { YesNoDialogComponent } from '../../dialogs/yes-no-dialog/yes-no-dialog.component';
import { OperationState, PainIndicatorDTO } from '../../../../generated';

@Component({
  selector: 'pdx-pain-documentation-page',
  templateUrl: './pain-localisation-page.component.html',
  styleUrls: ['./pain-localisation-page.component.scss'],
  host: {class: 'apply_flex'}
})
export class PainLocalisationPageComponent implements OnInit, AfterViewInit {

  private static PERSON_SOURCE = '/assets/person.svg';

  @ViewChild('canvasContainer') canvasContainer: HTMLElement;
  @ViewChild('stage') stageElement: KonvaComponent;
  @ViewChild('bgLayer') bgLayerElement: KonvaComponent;
  @ViewChild('drawLayer') drawLayerElement: KonvaComponent;
  @ViewChild('transformLayer') transformLayerElement: KonvaComponent;

  public get sideBarActions(): SideBarAction[]  {
    const actions = [];
    if (this.showViewAction) {
      actions.push(new SideBarAction('search', this.onEditSelection.bind(this)));
    }
    if (this.showEditActions) {
      actions.push(new SideBarAction('delete', this.onDeleteSelection.bind(this)));
      actions.push(new SideBarAction('edit', this.onEditSelection.bind(this)));
    }
    return actions;
  }

  public configStage: Observable<any> = of({
    width: 600,
    height: 400
  });

  public get showViewAction(): boolean {
    return this.hasSelection && !this.editable;
  }

  public get showEditActions(): boolean {
    return this.hasSelection && this.editable;
  }


  private indicatorDtoByNodes: Map<Konva.Node, PainIndicatorDTO> = new Map<Konva.Node, PainIndicatorDTO>();

  private stage: Konva.Stage;
  private bgLayer: Konva.Layer;
  private drawLayer: Konva.Layer;
  private transformLayer: Konva.Layer;
  private background: Konva.Image;

  private canvasWidth: number;
  private canvasHeight: number;

  private bgNaturalWidth: number;
  private bgNaturalHeight: number;

  private bgXOffset: number;
  private bgYOffset: number;
  private bgWidth: number;
  private bgHeight: number;

  private isDrawing = false;
  private startPos: Konva.Vector2d;
  private lastCircle: Konva.Circle | null;
  private transformer: Konva.Transformer;
  private selectionRectangle: Konva.Rect;

  private beforeSaveEventSubscription: Subscription;
  private painIndicatorDialogRef: MatDialogRef<PainIndicatorDialogComponent> | undefined = undefined;

  private get scalingFactor(): number {
    return this.bgWidth / this.bgNaturalWidth;
  }

  private get normalizingFactor(): number {
    return this.bgNaturalWidth / this.bgWidth;
  }

  private get editable(): boolean {
    return this.operationsService.getActOperation().state === OperationState.INPROGRESS;
  }

  private get hasSelection(): boolean {
    return this.transformer && this.transformer.getNodes().length > 0;
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.keyCode === Key.Delete) {
      if (this.transformer) {
        this.onDeleteSelection();
      }
    }
  }

  constructor(
    private operationsService: OperationsService,
    private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.transformer = new Konva.Transformer({
      centeredScaling: true,
      keepRatio: true,
      rotateEnabled: this.editable,
      resizeEnabled: this.editable,
      enabledAnchors: this.editable ? undefined : []
    });
  }

  ngAfterViewInit() {
    this.stage = this.stageElement.getStage();
    this.bgLayer = this.bgLayerElement.getStage();
    this.drawLayer = this.drawLayerElement.getStage();
    this.transformLayer = this.transformLayerElement.getStage();

    Konva.Image.fromURL(PainLocalisationPageComponent.PERSON_SOURCE, (imageNode: Konva.Image) => {
      this.background = imageNode;
      this.bgLayer.add(this.background);
      this.updateBackgroundDimensions();
      this.loadPainIndicators();
    });

    this.transformLayer.add(this.transformer);

    this.stage.on('mousedown touchstart', this.onStageMouseDown.bind(this));
    this.stage.on('mouseup touchend', this.onStageMouseUp.bind(this));
    this.stage.on('mousemove touchmove', this.onStageMouseMove.bind(this));
    this.stage.on('click tap', this.onStageClick.bind(this));
    this.stage.on('dblclick dbltap', this.onStageDoubleClick.bind(this));
    this.drawLayer.on('dragend', this.onDrawLayerDragEnd.bind(this));

    this.transformer.addEventListener('transformend', () => this.onShapeTransformed());
  }

  public onDeleteSelection() {
    if (this.dialog.openDialogs && this.dialog.openDialogs.length > 0) {
      return;
    }
    const shape = this.transformer.getNodes()[0];
    const dto = this.indicatorDtoByNodes.get(shape);
    if (dto) {
      const yesNoDialogRef = this.dialog.open(YesNoDialogComponent, {
        width: '400px',
        data: {title: 'Wollen Sie den Schmerzindikator wirklich löschen?', content: 'Beschreibung: \n' + dto.description }
      });
      yesNoDialogRef.afterClosed().subscribe(proceed => {
        if (proceed) {
          const indicators = this.operationsService.getActOperation().painLocalisation.indicators;
          const index = indicators.indexOf(dto);
          indicators.splice(index, 1);
          this.indicatorDtoByNodes.delete(shape);
          shape.destroy();
          this.transformer.nodes([]);
        }
      });
    }
  }

  public onEditSelection() {
    this.editShapeDto(this.transformer.getNodes()[0]);
  }

  private editShapeDto(shape: Konva.Shape | Konva.Node) {
    this.painIndicatorDialogRef = this.dialog.open(PainIndicatorDialogComponent, {
      width: '400px',
      data: this.indicatorDtoByNodes.get(shape)
    });

    this.painIndicatorDialogRef.afterClosed().subscribe(result => {
      this.painIndicatorDialogRef = undefined;
    });
  }

  /*  NORMALIZATION AND SERIALIZATION  */

  private loadPainIndicators() {
    const operation = this.operationsService.getActOperation();
    // console.log('load pain indicators %o', operation.painLocalisation.indicators);
    operation.painLocalisation.indicators?.forEach((indicator: PainIndicatorDTO) => {
      const shape = Konva.Shape.create(indicator.shape);
      let attributes = {};
      switch (shape.className) {
        case 'Circle':
          attributes = {
            x: shape.getX() * this.scalingFactor + this.bgXOffset,
            y: shape.getY() * this.scalingFactor + this.bgYOffset,
            radius: shape.getRadius() * this.scalingFactor,
            draggable: this.editable
          };
          break;
      }
      // console.log('load attributes %o', attributes);
      shape.setAttrs(attributes);
      this.indicatorDtoByNodes.set(shape, indicator);
      this.drawLayer.add(shape);
    });
  }

  private savePainIndicators() {
    const operation = this.operationsService.getActOperation();
    const indicators: PainIndicatorDTO[] = [];
    this.indicatorDtoByNodes.forEach((indicator, shape) => {
      const normalizedShape = this.getNormalizedShape(shape);
      indicator.shape = normalizedShape.toJSON();
      indicators.push(indicator);
    });
    // console.log('saved pain indicators: %o', indicators);
    operation.painLocalisation.indicators = indicators;
  }

  private getNormalizedShape(shape: Konva.Shape | Konva.Node): Konva.Shape {
    const normalizedShape = Konva.Shape.create(shape);
    let attributes = {};
    switch (shape.className) {
      case 'Circle':
        attributes = {
          x: (normalizedShape.getX() - this.bgXOffset) * this.normalizingFactor,
          y: (normalizedShape.getY() - this.bgYOffset) * this.normalizingFactor,
          radius: normalizedShape.getRadius() * this.normalizingFactor
        };
        // console.log('save attributes %o', attributes);
        break;
    }
    normalizedShape.setAttrs(attributes);
    return normalizedShape;
  }

  /*  MOUSE / GESTURE EVENTS  */

  private onShapeTransformed() {
    this.updateExistingShape(this.transformer.getNodes()[0]);
  }

  private onDrawLayerDragEnd(e: KonvaEventObject<any>) {
    // @ts-ignore   wrong type definition in library
    if (e.target !== this.transformer) {
      this.updateExistingShape(e.target as Shape);
    }
  }

  private updateExistingShape(shape: Konva.Shape | Konva.Node) {
    const dto = this.indicatorDtoByNodes.get(shape);
    if (dto) {
      dto.shape = this.getNormalizedShape(shape).toJSON();
    }
  }

  private onStageMouseDown(e: KonvaEventObject<any>) {
    if (!this.isTargetBackground(e.target) || !this.editable) {
      return;
    }
    this.isDrawing = true;
    const pos = this.stage.getPointerPosition();
    if (pos) {
      this.startPos = pos;
    }
  }

  private onStageMouseUp(e: KonvaEventObject<any>) {
    this.isDrawing = false;
    if (this.lastCircle) {
      const dto = <PainIndicatorDTO>{
        description: '',
        shape: this.getNormalizedShape(this.lastCircle).toJSON()
      };
      this.indicatorDtoByNodes.set(this.lastCircle, dto);
      // console.log('added new pain indicator');
      this.operationsService.getActOperation().painLocalisation.indicators.push(dto);
      this.lastCircle = null;
    }
  }

  private onStageMouseMove(e: KonvaEventObject<any>) {
    const pos = this.stage.getPointerPosition();
    if (!pos) { return; }
    if (this.isDrawing) {
      const distance = Math.hypot(pos.x - this.startPos.x, pos.y - this.startPos.y);
      if (distance > 5) {
        if (this.lastCircle) {
          this.lastCircle.radius(distance);
        } else {
          this.lastCircle = new Konva.Circle({
            x: pos.x,
            y: pos.y,
            fill: 'red',
            opacity: 0.6,
            strokeWidth: 5,
            radius: 5,
            draggable: true
          });
          this.drawLayer.add(this.lastCircle);
        }
      }
    }
  }

  private onStageClick(e: KonvaEventObject<any>) {
    if (this.isTargetBackground(e.target)) {
      this.transformer.nodes([]);
      return;
    }
    this.transformer.nodes([e.target]);
  }

  private onStageDoubleClick(e: KonvaEventObject<any>) {
    if (this.isTargetBackground(e.target)) {
      return;
    }
    this.editShapeDto(e.target);
  }

  private isTargetBackground(target: Konva.Shape | Konva.Stage) {
    return target === this.stage || target === this.background;
  }


  /*  RESPONSIVENESS  */

  public onCanvasContainerResized(event: ResizedEvent) {
    // console.log('resized w: %o | h %o', event.newWidth, event.newHeight);
    this.canvasWidth = event.newWidth;
    this.canvasHeight = event.newHeight;
    this.stage.width(this.canvasWidth);
    this.stage.height(this.canvasHeight);

    if (this.background) {
      this.savePainIndicators();

      this.drawLayer.destroyChildren();
      this.drawLayer.clear();
      this.indicatorDtoByNodes.clear();

      this.updateBackgroundDimensions();
      this.loadPainIndicators();
    }
  }

  private updateBackgroundDimensions() {
    const image = this.background.image() as HTMLImageElement;
    if (image) {
      this.bgNaturalWidth = image.naturalWidth;
      this.bgNaturalHeight = image.naturalHeight;

      let bgHeight = this.canvasHeight;
      let bgWidth = this.bgNaturalWidth * this.canvasHeight / this.bgNaturalHeight;
      if (bgWidth > this.canvasWidth) {
        bgWidth = this.canvasWidth;
        bgHeight = this.bgNaturalHeight * this.canvasWidth / this.bgNaturalWidth;
      }
      const x = (this.canvasWidth - bgWidth) / 2;
      const y = (this.canvasHeight - bgHeight) / 2;

      this.bgXOffset = x;
      this.bgYOffset = y;
      this.bgWidth = bgWidth;
      this.bgHeight = bgHeight;

      this.background.setAttrs({
        x: x,
        y: y,
        width: bgWidth,
        height: bgHeight
      });
    }
  }
}
