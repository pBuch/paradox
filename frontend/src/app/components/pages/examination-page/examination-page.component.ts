import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { QuickScrollDestination } from '../../../util/quick-scroll-destination';

@Component({
  selector: 'pdx-examination-page',
  templateUrl: './examination-page.component.html',
  styleUrls: ['./examination-page.component.scss'],
  host: {class: 'apply_flex'},
})
export class ExaminationPageComponent implements OnInit, AfterViewInit {

  @ViewChild('patientPositionAssessment', {read: ElementRef}) patientPositionAssessmentRef: ElementRef;
  @ViewChild('respirationAssessment', {read: ElementRef}) respirationAssessmentRef: ElementRef;
  @ViewChild('circulatoryAssessment', {read: ElementRef}) circulatoryAssessmentRef: ElementRef;
  @ViewChild('patientCritical', {read: ElementRef}) patientCriticalRef: ElementRef;
  @ViewChild('neurologicalAssessment', {read: ElementRef}) neurologicalAssessmentRef: ElementRef;
  @ViewChild('furtherAssessment', {read: ElementRef}) furtherAssessmentRef: ElementRef;


  public quickScrollDestinations: QuickScrollDestination[];

  constructor(private cdRef: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.quickScrollDestinations = [];
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.quickScrollDestinations = [
        new QuickScrollDestination('assets/icons/patient.svg', this.patientPositionAssessmentRef, true),
        new QuickScrollDestination('assets/icons/breathing.svg', this.respirationAssessmentRef, false),
        new QuickScrollDestination('assets/icons/heart-rate.svg', this.circulatoryAssessmentRef, false),
        new QuickScrollDestination('assets/icons/emergency-kit.svg', this.patientCriticalRef, false, true),
        new QuickScrollDestination('assets/icons/brain.svg', this.neurologicalAssessmentRef, false),
        new QuickScrollDestination('assets/icons/stethoscope.svg', this.furtherAssessmentRef, false)
      ];
      this.cdRef.detectChanges();
    }, 10);
  }

}
