import { Component, Input, OnInit } from '@angular/core';
import { MedicalAidOperationInfoDTO, OperationState } from '../../../generated';
import { OperationsService } from '../../services/operations.service';
import { Router } from '@angular/router';

@Component({
  selector: 'pdx-operation-list',
  templateUrl: './operation-list.component.html',
  styleUrls: ['./operation-list.component.scss']
})
export class OperationListComponent {

  @Input() operations: MedicalAidOperationInfoDTO[];

  operationState = OperationState;

  constructor(private operationsService: OperationsService,
    private router: Router) {
  }

  onClickOperation(operation: MedicalAidOperationInfoDTO) {
    this.operationsService.chooseOperation(operation);
    this.router.navigateByUrl('/basic-info');
  }
}
