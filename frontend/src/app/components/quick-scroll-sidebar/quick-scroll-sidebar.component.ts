import { Component, OnInit, Input, HostListener, ElementRef } from '@angular/core';
import { QuickScrollDestination } from '../../util/quick-scroll-destination';
import { ScrollToService } from '@nicky-lenaers/ngx-scroll-to';
import { OperationsService } from '../../services/operations.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Globals } from '../../global/globals';
import { MedicalAidOperationInfoDTO, OperationState } from '../../../generated';
import { Router } from '@angular/router';
import { ErrorDialogComponent } from '../dialogs/error-dialog/error-dialog.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { SideBarAction } from '../../util/side-bar-action';

@Component({
  selector: 'pdx-quick-scroll-sidebar',
  templateUrl: './quick-scroll-sidebar.component.html',
  styleUrls: ['./quick-scroll-sidebar.component.scss']
})
export class QuickScrollSidebarComponent {


  private _quickScrollDestinations: QuickScrollDestination[];
  private _lowestScrollDestination: QuickScrollDestination;

  @Input()
  get quickScrollDestinations(): QuickScrollDestination[] {
    return this._quickScrollDestinations;
  }
  set quickScrollDestinations(qsds: QuickScrollDestination[]) {
    if (!qsds || qsds.length === 0) {
      this._quickScrollDestinations = [];
      return;
    }
    qsds = qsds.sort((q1, q2) => q1.offsetTop - q2.offsetTop);
    this.generateScrollThresholds(qsds);
    this._quickScrollDestinations = qsds;
  }

  @Input()
  public sideBarActions: SideBarAction[];

  constructor(private scrollToService: ScrollToService,
    private operationsService: OperationsService,
    private snackBar: MatSnackBar,
    private router: Router,
    public dialog: MatDialog) {
  }

  get isFinished(): boolean {
    return this.operationsService.getActOperation().state === OperationState.COMPLETED;
  }

  public onScrollButtonClicked(qsd: QuickScrollDestination) {
    let offset = qsd.scrollPosition - window.scrollY + 1;
    if (qsd === this._lowestScrollDestination) {
      offset = qsd.offsetTop + qsd.offsetHeight - window.innerHeight;
    }
    const scrollConfig = {
      offset: offset
    };

    this.scrollToService.scrollTo(scrollConfig);
  }

  onFinishClicked() {
    this.operationsService.completeOperation().subscribe(
      result => {
        if ((result as MedicalAidOperationInfoDTO).state != null) {
          this.snackBar.open('Einsatz abgeschlossen!');
          this.router.navigateByUrl('/');
        }
      },
      errors => {
        const dialogRef = this.dialog.open(ErrorDialogComponent, {
          width: '400px',
          data: {errors: errors}
        });
      });
  }

  onSaveBtnClicked() {
    this.operationsService.saveActOperation().subscribe(() => {
      this.snackBar.open('Erfolgreich gespeichert!');
    });
  }

  @HostListener('window:scroll', ['$event'])
  onScroll(event: any) {
    if (!this.quickScrollDestinations) { return; }
    const y = window.scrollY;
    // console.log(y)
    let activeSet = false;
    for (let i = this.quickScrollDestinations.length - 1; i >= 0; i--) {
      const q = this.quickScrollDestinations[i];
      if (!activeSet && y >= q.scrollPosition) {
        q.active = true;
        activeSet = true;
      } else {
        q.active = false;
      }
    }
  }

  private generateScrollThresholds(qsds: QuickScrollDestination[]) {
    qsds[0].scrollPosition = 0;

    const lowestPoint = qsds[qsds.length - 1].offsetTop + qsds[qsds.length - 1].offsetHeight;
    const lowestScrollPoint = lowestPoint - window.innerHeight;

    // console.log(window.innerHeight);
    // console.log(lowestScrollPoint);
    for (let i = 0; i < qsds.length - 1; i++) {
      const diff = qsds[i + 1].offsetTop - qsds[i].offsetTop;
      // console.log('offset top: %o | diff %o', qsds[i + 1].offsetTop, diff);
      let threshold = qsds[i + 1].offsetTop - diff / 2;
      if (threshold > lowestScrollPoint) {
        threshold = lowestScrollPoint - 100 * (qsds.length - i - 1);
      }
      qsds[i + 1].scrollPosition = threshold;
    }
    for (let i = qsds.length - 1; i >= 1; i--) {
      if (qsds[i].scrollPosition < qsds[i - 1].scrollPosition) {
        qsds[i - 1].scrollPosition = qsds[i].scrollPosition - 100;
      }
    }
    this._lowestScrollDestination = qsds[qsds.length - 1];
    // console.log(qsds);
  }

}
